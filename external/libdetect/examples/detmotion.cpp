/***************************************************************************
 *   Copyright (C) 2011 by Nicola Bellotto                                 *
 *   nbellotto@lincoln.ac.uk                                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <libplayerc++/playerc++.h>
#include "motiondetector.h"
#include "utility.h"
#include <iostream>


using namespace PlayerCc;
using namespace std;


int main(int argc, char* argv[])
{
  const char* address = "localhost";

  if (argc > 1)
    address = argv[1];

  try {
    PlayerClient* robot = new PlayerClient(address);
    LaserProxy* laser = new LaserProxy(robot, 0);

    // wait for sensors to be ready
    cout << "Wait for sensors";
    int attempts = 0;
    while (!laser->IsValid()) {
      robot->Read();
      if (++attempts > 10) {
        cout << " sensors not ready! Exit." << endl;
        exit(EXIT_FAILURE);
      }
      cout << ".";
      sleep(1);
    }
    cout << " connected!" << endl;

    // initialize
    int laserSize = laser->GetCount();
    vector< laser_t > laserBuf(laserSize);

    MotionDetector* md = new MotionDetector(laserSize, 3);
    md->setDebug(true);

    while ((uchar)cvWaitKey(10) != 27) {
      // update data
      robot->Read();

      if (laser->IsFresh()) {
        // now read the laser
        for (int li = 0; li < laserSize; li++) {
            laserBuf[li].range = laser->GetRange(li);
            laserBuf[li].angle = laser->GetBearing(li); // always the same...
            player_point_2d_t p = laser->GetPoint(li);
            laserBuf[li].x = p.px;
            laserBuf[li].y = p.py;
            laserBuf[li].intensity = laser->GetIntensity(li); // needs IntensityOn() to work...
        }

        // update detectors
        md->update(laserBuf);
        laser->NotFresh();
      }

      // printout results
      for (int n = md->getHowMany(); n > 0; n--) {
        double dist = md->getDistance(n-1);
        double bear = md->getBearing(n-1);

        printf("Person %d - distance = %.2f[m] - bearing = %.2f[deg]\n",
               n,
               dist,
               RTOD(bear));
        std::cout << "-----------------------------\n";
      }

    }

    delete md;
    delete laser;
    delete robot;
  }
  catch (PlayerError& e) {
    cerr << "PlayerError: " << e.GetErrorStr() << endl;
  }

  return EXIT_SUCCESS;
}
