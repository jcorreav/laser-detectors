/***************************************************************************
 *   Copyright (C) 2011 by Nicola Bellotto                                 *
 *   nbellotto@lincoln.ac.uk                                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <libplayerc++/playerc++.h>
#include "facedetector.h"
#include "utility.h"
#include <iostream>


using namespace PlayerCc;
using namespace std;

int main(int argc, char* argv[])
{
  const char* address = "localhost";

  if (argc > 1)
    address = argv[1];

  try {
    PlayerClient* robot = new PlayerClient(address);
    CameraProxy* camera = new CameraProxy(robot, 0);

    // wait for sensors to be ready
    cout << "Wait for sensors";
    int attempts = 0;
    while (!camera->IsValid()) {
      robot->Read();
      if (++attempts > 10) {
        cout << " sensors not ready! Exit." << endl;
        exit(EXIT_FAILURE);
      }
      cout << ".";
      sleep(1);
    }
    cout << " connected!" << endl;

    // initialize
    int camWidth = camera->GetWidth();
    int camHeight = camera->GetHeight();
    int camDepth = camera->GetDepth();
    cout << "camera: " << camWidth << "x" << camHeight << "x" << camDepth << endl;
    uint8_t frameBuf[camWidth*camHeight*3];
    IplImage* img = cvCreateImage(cvSize(camWidth, camHeight), IPL_DEPTH_8U, 3);

    FaceDetector* fd = new FaceDetector(camWidth, camHeight, 3,
                                        415, 415 /*for Logitech QuickCam*/,
                                        "/usr/local/share/opencv/haarcascades/haarcascade_frontalface_alt2.xml");
    fd->setDebug(true);

    while ((uchar)cvWaitKey(10) != 27) {
      // update data
      robot->Read();

      if (camera->IsFresh()) {
        // read the camera
        camera->GetImage(frameBuf);
        // get data
        for (int x = 0; x < camWidth; x++) {
            for (int y = 0; y < camHeight; y++) {
              // the camera's image is RGB but OpenCV wants BGR
              ((uchar*)(img->imageData + img->widthStep * y))[x * 3] =
                  (uchar)(frameBuf[3*camWidth * y + x * 3 + 2]);
              ((uchar*)(img->imageData + img->widthStep * y))[x * 3 + 1] =
                  (uchar)(frameBuf[3*camWidth * y + x * 3 + 1]);
              ((uchar*)(img->imageData + img->widthStep * y))[x * 3 + 2] =
                  (uchar)(frameBuf[3*camWidth * y + x * 3]);
            }
        }
        fd->update(img);
        camera->NotFresh();
      }

      // printout results
      for (int n = fd->getHowMany(); n > 0; n--) {
        double bear = fd->getBearing(n-1);
        double elev = fd->getElevation(n-1);

        printf("Person %d - bearing = %.2f[deg] - elevation = %.2f[deg]\n",
               n,
               RTOD(bear),
               RTOD(elev));
        std::cout << "-----------------------------\n";
      }

    }

    delete fd;
    delete camera;
    delete robot;
  }
  catch (PlayerError& e) {
    cerr << "PlayerError: " << e.GetErrorStr() << endl;
  }

  return EXIT_SUCCESS;
}
