/***************************************************************************
 *   Copyright (C) 2011 by Nicola Bellotto                                 *
 *   nbellotto@lincoln.ac.uk                                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "polygon.h"
#include <iostream>
#include <errno.h>
#include <stdlib.h>

#define  ERROR(msg)  { std::cerr << "ERROR in " << __FILE__ << ":" << __LINE__ \
                                 << "\n\t" << msg << "\n"; }
#define  EXIT(msg,err)  { ERROR(msg); exit(err); }

Polygon::Polygon(int numOfContours)
{
   if (numOfContours < 1)
      EXIT("Number of contours must be at least 1", EINVAL);
   Poly = new gpc_polygon;
   Poly->num_contours = NumOfContours = numOfContours;
   Poly->hole = new int[NumOfContours];
   Poly->contour = new gpc_vertex_list[NumOfContours];
   // initialize empty contours
   for (int c = 0; c < NumOfContours; c++)
   {
      Poly->hole[c] = 0;
      Poly->contour[c].num_vertices = 0;
      Poly->contour[c].vertex = NULL;
   }
}


Polygon::Polygon(const Polygon& polygon)
{
   Poly = new gpc_polygon;
   Poly->num_contours = NumOfContours = polygon.NumOfContours;
   Poly->hole = new int[NumOfContours];
   Poly->contour = new gpc_vertex_list[NumOfContours];
   // initialize contours
   for (int c = 0; c < NumOfContours; c++)
   {
      Poly->hole[c] = polygon.Poly->hole[c];
      int n = polygon.Poly->contour[c].num_vertices;
      Poly->contour[c].num_vertices = n;
      // copy all the vertices
      if (n > 0)
      {
         Poly->contour[c].vertex = new gpc_vertex[n];
         gpc_vertex* v_dest = Poly->contour[c].vertex;
         gpc_vertex* v_orig = polygon.Poly->contour[c].vertex;
         for (int i = 0; i < n; i++)
            v_dest[i] = v_orig[i];
      }
   }
}

Polygon::Polygon(gpc_polygon* gpcPolygon)
{
   Poly = new gpc_polygon;
   Poly->num_contours = NumOfContours = gpcPolygon->num_contours;
   Poly->hole = new int[NumOfContours];
   Poly->contour = new gpc_vertex_list[NumOfContours];
   // initialize contours
   for (int c = 0; c < NumOfContours; c++)
   {
      Poly->hole[c] = gpcPolygon->hole[c];
      int n = gpcPolygon->contour[c].num_vertices;
      Poly->contour[c].num_vertices = n;
      // copy all the vertices
      if (n > 0)
      {
         Poly->contour[c].vertex = new gpc_vertex[n];
         gpc_vertex* v_dest = Poly->contour[c].vertex;
         gpc_vertex* v_orig = gpcPolygon->contour[c].vertex;
         for (int i = 0; i < n; i++)
            v_dest[i] = v_orig[i];
      }
   }
}


void clean(gpc_polygon* gpcPolygon)
{
   for (int c = 0; c < gpcPolygon->num_contours; c++)
   {
      if (gpcPolygon->contour[c].num_vertices > 0)
         delete[] gpcPolygon->contour[c].vertex;
   }

   delete[] gpcPolygon->hole;
   delete[] gpcPolygon->contour;
}


Polygon::~Polygon()
{
   clean(Poly);
   delete Poly;
}


void Polygon::setContour(int contourNum, int numOfVertices, bool isHole)
{
   if ((contourNum < 0) || (contourNum >= Poly->num_contours))
      EXIT("Wrong contour's number", EINVAL);

   // delete previous vertices
   if (Poly->contour[contourNum].num_vertices > 0)
      delete[] Poly->contour[contourNum].vertex;

   // allocate array for new vertices
   Poly->contour[contourNum].num_vertices = numOfVertices;
   Poly->contour[contourNum].vertex = new gpc_vertex[numOfVertices];

   Poly->hole[contourNum] = isHole ? 1 : 0;
}


int Polygon::getNumOfContours()
{
   return Poly->num_contours;
}


int Polygon::getNumOfVertices(int contourNum)
{
   if ((contourNum < 0) || (contourNum >= Poly->num_contours))
      EXIT("Wrong contour's number", EINVAL);

   return Poly->contour[contourNum].num_vertices;
}


bool Polygon::isHole(int contourNum)
{
   if ((contourNum < 0) || (contourNum >= Poly->num_contours))
      EXIT("Wrong contour's number", EINVAL);

   return Poly->hole[contourNum] ? true : false;
}


gpc_vertex* Polygon::getVertexArray(int contourNum)
{
   if ((contourNum < 0) || (contourNum >= Poly->num_contours))
      EXIT("Wrong contour's number", EINVAL);

   return Poly->contour[contourNum].vertex;
}


Polygon Polygon::operator= (const Polygon& polygon)
{
   return Polygon(polygon);
}


Polygon Polygon::operator|= (const Polygon& polygon)
{
   gpc_polygon* result = new gpc_polygon;
   gpc_polygon_clip(GPC_UNION, Poly, polygon.Poly, result);
   clean(Poly);
   delete Poly;
   Poly = result;
   NumOfContours = Poly->num_contours;
   return *this;
}


Polygon operator| (const Polygon& polygon_1, const Polygon& polygon_2)
{
   Polygon p(polygon_1);
   p |= polygon_2;
   return p;
}


Polygon Polygon::operator&= (const Polygon& polygon)
{
   gpc_polygon* result = new gpc_polygon;
   gpc_polygon_clip(GPC_INT, Poly, polygon.Poly, result);
   clean(Poly);
   delete Poly;
   Poly = result;
   NumOfContours = Poly->num_contours;
   return *this;
}


Polygon operator& (const Polygon& polygon_1, const Polygon& polygon_2)
{
   Polygon p(polygon_1);
   p &= polygon_2;
   return p;
}


Polygon Polygon::operator-= (const Polygon& polygon)
{
   gpc_polygon* result = new gpc_polygon;
   gpc_polygon_clip(GPC_DIFF, Poly, polygon.Poly, result);
   clean(Poly);
   delete Poly;
   Poly = result;
   NumOfContours = Poly->num_contours;
   return *this;
}


Polygon operator- (const Polygon& polygon_1, const Polygon& polygon_2)
{
   Polygon p(polygon_1);
   p -= polygon_2;
   return p;
}


// Original code from:
// http://local.wasp.uwa.edu.au/~pbourke/geometry/insidepoly/
bool Polygon::isInside(const gpc_vertex& p)
{
   bool inside = false;
   for (int c = 0; c < NumOfContours; c++)
   {
      int npol = Poly->contour[c].num_vertices;
      for (int i = 0, j = npol-1; i < npol; j = i++)
      {
         gpc_vertex p1 = Poly->contour[c].vertex[i];
         gpc_vertex p2 = Poly->contour[c].vertex[j];
         if ((((p1.y <= p.y) && (p.y < p2.y)) ||
              ((p2.y <= p.y) && (p.y < p1.y))) &&
             (p.x < (p2.x - p1.x) * (p.y - p1.y) / (p2.y - p1.y) + p1.x))
            inside = !inside;
      }
   }
   return inside;
}
