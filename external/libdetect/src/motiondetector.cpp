/***************************************************************************
 *   Copyright (C) 2011 by Nicola Bellotto                                 *
 *   nbellotto@lincoln.ac.uk                                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "motiondetector.h"

#include "polygon.h"
#include <iostream>
#include <errno.h>
#include <vector>


const double MAXIMUM_RANGE = 20.;      // maximum detection range, in [m]
const double MAP_RESOLUTION = 0.10;    // [m]
// for grid map
#define M2P(m) ((int)((float)(m) / (MAP_RESOLUTION) + 0.5))
#define P2M(p) ((float)(p) * (MAP_RESOLUTION))
const int MAP_WIDTH = 2 * M2P(MAXIMUM_RANGE);
const int MAP_HEIGHT = 2 * M2P(MAXIMUM_RANGE);
// for debugging
const double DEBUG_RESOLUTION = 0.02;   // [m]
#define METER2PIXEL(m) ((int)((float)(m) / (DEBUG_RESOLUTION) + 0.5))
#define PIXEL2METER(p) ((float)(p) * (DEBUG_RESOLUTION))
const double DEBUG_SCALE = 1.00;       // debug image size scaling
const int DEBUG_WINDOW_WIDTH = 320;
const int DEBUG_WINDOW_HEIGHT = 240;
const int TMP_WINDOW_WIDTH = 2 * METER2PIXEL(MAXIMUM_RANGE);
const int TMP_WINDOW_HEIGHT = 2 * METER2PIXEL(MAXIMUM_RANGE);


MotionDetector::MotionDetector(int bufferLength, int steps, double maxDistance)
{
   _bufferLength = bufferLength;
   _storage = cvCreateMemStorage(0);
   _point32fSeq = cvCreateSeq(CV_32FC2, sizeof(CvSeq), sizeof(CvPoint2D32f), _storage);
   _pointSeq = cvCreateSeq(CV_8UC1, sizeof(CvSeq), sizeof(CvPoint), _storage);
   _mapImg = cvCreateImage(cvSize((int)(MAP_WIDTH),
                                  (int)(MAP_HEIGHT)),
                                  IPL_DEPTH_8U, 1);
   cvSetZero(_mapImg);
   _oldGrid = cvCloneImage(_mapImg);
   _newGrid = cvCloneImage(_mapImg);

   _howMany = 0;

   _steps = steps;

   _maxDistance = maxDistance;
   
   _debug = false;
}


MotionDetector::~MotionDetector()
{
   cvReleaseMemStorage(&_storage);
   cvReleaseImage(&_mapImg);
   cvReleaseImage(&_oldGrid);
   cvReleaseImage(&_newGrid);

   // deallocate set of free-spaces
   deque< gpc_polygon* >::iterator itend = _S.end();
   for (deque< gpc_polygon* >::iterator it = _S.begin(); it != itend; it++) {
      gpc_free_polygon(*it);
      delete (*it);
   }

   int n = _Space.size();
   for (int i = 0; i < n; i++)
      delete _Space[i];   // delete polygon's pointer
   
   if (_debug) {
      cvReleaseImage(&_debugImage);
      cvReleaseImage(&_tmpImg);
      cvReleaseImage(&_grayImg);
      cvDestroyWindow("Motion detector");
   }
}


// comparison function for points clustering
int is_equal( const void* _a, const void* _b, void* userdata )
{
    CvPoint a = *(const CvPoint*)_a;
    CvPoint b = *(const CvPoint*)_b;
    int threshold = *(int*)userdata;
    return (a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y) <= threshold;
}

void MotionDetector::updateNew(const std::vector< laser_t >& laserBuffer, double x0, double y0, double th0)
{
   static int clusterThreshold = M2P(0.5*0.5); // threshold for point clustering (radius square, both legs)
   static const int pointThreshold = 10;     // threshold for point clustering (min number of poins)
   
   vector< point_t > targetVec;  // vector of target points
   CvPoint pt;
   
   cvClearSeq(_pointSeq);
   cvSetZero(_newGrid);

   std::vector< laser_t >::const_iterator lit, lend = laserBuffer.end();
   for (lit = laserBuffer.begin(); lit != lend; lit++) {
      pt.x = M2P(lit->y) + MAP_WIDTH/2;
      pt.y = M2P(lit->x) + MAP_HEIGHT/2;
      ((uchar *)(_newGrid->imageData + pt.y*_newGrid->widthStep))[pt.x]=255;
//       pt.x = M2P(lit->range * cos(lit->angle + th0) + x0);
//       pt.y = M2P(lit->range * sin(lit->angle + th0) + y0);
      cvSeqPush(_pointSeq, &pt);
   }
   cvXor(_oldGrid, _newGrid, _mapImg);
   // swap old and new grid pointers
   IplImage* dumImg = _oldGrid;
   _oldGrid = _newGrid;
   _newGrid = dumImg;

   if (_debug) {
      cvSet(_tmpImg, cvScalar(0,0,0)); // clear debug image
      // draw grid map
      cvResize(_mapImg, _grayImg, CV_INTER_NN);
      cvCvtColor(_grayImg, _tmpImg, CV_GRAY2RGB);
      // draw motion points
      CvPoint p;
      CvSeqReader reader;
      cvStartReadSeq(_pointSeq, &reader, 0 );
      for(int i = 0; i < _pointSeq->total; i++) {
         CvPoint& pt = *(CvPoint*)reader.ptr;
         p.x = METER2PIXEL(P2M(pt.y)) + TMP_WINDOW_WIDTH/2;
         p.y = METER2PIXEL(P2M(pt.x)) + TMP_WINDOW_HEIGHT/2;
//          cvSet2D(_tmpImg, p.y, p.x, cvScalar(255,255,255));
         CV_NEXT_SEQ_ELEM(_pointSeq->elem_size, reader);
      }
   }
   
   // (from OpenCV source code...)
   // the function splits the input sequence or set into one or more equivalence classes.
   // is_equal(a,b,...) returns non-zero if the two sequence elements
   // belong to the same class. the function returns sequence of integers -
   // 0-based class indexes for each element.
   //
   // The algorithm is described in "Introduction to Algorithms"
   // by Cormen, Leiserson and Rivest, chapter "Data structures for disjoint sets"
   CvSeq* labels = 0;
   int classNum = cvSeqPartition(_pointSeq, 0, &labels, is_equal, &clusterThreshold);

   // calculate centre of each cluster
   vector< vector< CvPoint* > > table(classNum);
   for(int l = 0; l < labels->total; l++)
      table[*(int*)cvGetSeqElem(labels, l)].push_back((CvPoint*)cvGetSeqElem(_pointSeq, l));
      
   for (int c = 0; c < classNum; c++) {
      int pointNum = table[c].size();
      if (pointNum < pointThreshold)
         continue;   // do not even consider clusters too small
      vector< CvPoint* >::iterator pi, piEnd = table[c].end();
      int xSum = 0, ySum = 0;
      for (pi = table[c].begin(); pi != piEnd; pi++) {
         xSum += (*pi)->x;
         ySum += (*pi)->y;
      }
      targetVec.push_back(point_t(P2M(ySum - pointNum*MAP_HEIGHT/2)/pointNum,
                                  P2M(xSum - pointNum*MAP_WIDTH/2)/pointNum));
   }

   // finally calculate distance and direction of each horizontal line
   _target.clear();
   vector< point_t >::iterator ti, tiEnd = targetVec.end();
   for (ti = targetVec.begin(); ti != tiEnd; ti++) {
      target_t t;
      t.distance = ti->rad;
      // left PI/2, right -PI/2
      t.bearing = ti->th;
      // no height information of course...
      _target.push_back(t);
   }
   // final number of detected people
   _howMany = _target.size();
   // draw the last things for debugging
   if (_debug) {
      // draw moving points
      for (int i = 0; i < _howMany; i++) {
         CvPoint p = cvPoint(METER2PIXEL(targetVec[i].y) + TMP_WINDOW_WIDTH/2,
            METER2PIXEL(targetVec[i].x) + TMP_WINDOW_HEIGHT/2);
//          cvLine(_tmpImg, cvPoint(TMP_WINDOW_WIDTH/2, TMP_WINDOW_HEIGHT/2), p, cvScalar(0,128,255), 2);
      }
      
      cvFlip(_tmpImg, NULL, -1);
      CvMat subImg;
      cvGetSubRect(_tmpImg, &subImg, cvRect(TMP_WINDOW_WIDTH/2 - (int)(DEBUG_WINDOW_WIDTH*DEBUG_SCALE)/2,
                                            TMP_WINDOW_HEIGHT/2 - (int)(DEBUG_WINDOW_HEIGHT*DEBUG_SCALE),
                                            (int)(DEBUG_WINDOW_WIDTH*DEBUG_SCALE),
                                            (int)(DEBUG_WINDOW_HEIGHT*DEBUG_SCALE)));
      cvResize(&subImg, _debugImage, CV_INTER_NN);
      cvShowImage("Motion detector", _debugImage);
      if (_delay)
        cvWaitKey(_delay);  // handles event processing of HIGHGUI library
   }
}


// comparison function for points clustering
int is_equal32f( const void* _a, const void* _b, void* userdata )
{
    CvPoint2D32f a = *(const CvPoint2D32f*)_a;
    CvPoint2D32f b = *(const CvPoint2D32f*)_b;
    double threshold = *(double*)userdata;
    return (double)(a.x - b.x)*(a.x - b.x) + (double)(a.y - b.y)*(a.y - b.y) <= threshold;
}
void MotionDetector::update(const std::vector< laser_t >& laserBuffer, double x0, double y0, double th0)
{
   // method's parameters
   static const int delta = 2;               // threshold for angle error smoothing
   static const int gamma = 5;               // threshold for additional angle error smoothing
   static const double lambda = 0.20;        // max gap between consecutive free spaces
   static const double mu = 1.0;             // weight of violation point distance
   static const double sigma2 = 0.250*0.250;   // variance of violation points cluster
   static const double threshold = 10.0;      // threshold on cluster distribution
   static double clusterThreshold = 0.5*0.5; // threshold for point clustering (radius square, two legs)
   static const int pointThreshold = 10;      // threshold for point clustering (min number of poins)

   vector< point_t > targetVec;  // vector of target points
   
   Polygon* space = new Polygon(1);
   space->setContour(0, _bufferLength+1);
   gpc_vertex* vertex = space->getVertexArray(0);
   // first vertex is robot position...
   vertex[0].x = x0;
   vertex[0].y = y0;
   // ... then laser points
   reading_t reading[_bufferLength];
   double min_range[_bufferLength];
   double copy_range[_bufferLength];
   for (int i = 0; i < _bufferLength; i++) {
      // bounded minimization filter
      min_range[i] = _maxDistance;
      for (int k = max(0, i-delta); k <= min(_bufferLength-1, i+delta); k++)
         if (laserBuffer[k].range < min_range[i])
            min_range[i] = laserBuffer[k].range;
      copy_range[i] = min_range[i];
      // save original (filtered) readings
      reading[i].range = min_range[i];
      reading[i].vertex.x = cos(laserBuffer[i].angle + th0) * min_range[i] + x0;
      reading[i].vertex.y = sin(laserBuffer[i].angle + th0) * min_range[i] + y0;
   }
   double prev_range = 0;
   for (int i = 0; i < _bufferLength; i++) {
      // additional filter, not present in original paper Lindstrom2001
      double diff = copy_range[i] - prev_range;
      prev_range = copy_range[i];
      if (diff > 0) {
         // rising edge, add angle
         for (int j = min(i+gamma, _bufferLength-1); j > i; j--)
            if (min_range[j] > copy_range[i])
               min_range[j] = copy_range[i];
      }
      else if (diff < 0) {
         // falling edge, subtract angle
         for (int j = max(i-gamma, 0); j < i; j++)
            if (min_range[j] > copy_range[i])
               min_range[j] = copy_range[i];
      }
      else {
         // peak
         for (int j = min(i+gamma, _bufferLength-1); j > i; j--)
            if (min_range[j] > copy_range[i])
               min_range[j] = copy_range[i];
         for (int j = max(i-gamma, 0); j < i; j++)
            if (min_range[j] > copy_range[i])
               min_range[j] = copy_range[i];
      }
      // (modified) free space's points
      vertex[i+1].x =
         cos(laserBuffer[i].angle + th0) * max(min_range[i] - lambda, 1e-6) + x0;
      vertex[i+1].y =
         sin(laserBuffer[i].angle + th0) * max(min_range[i] - lambda, 1e-6) + y0;
   }
   // calculate union of all previous polygons
   if (!_Space.empty()) {
      Polygon space_union = *_Space.front();  // initialize with last polygon added
      for (deque< Polygon* >::iterator it = _Space.begin()+1; it != _Space.end(); it++)
         space_union |= (**it);

      // calculate violation points
      vector< reading_t > V;
      for (int i = 0; i < _bufferLength; i++) {
         if (space_union.isInside(reading[i].vertex))
            V.push_back(reading[i]);
      }

      // filter violation points
      size_t vsize = V.size();
      double vfilt[vsize];
      cvClearSeq(_point32fSeq);
      for (size_t i = 0; i < vsize; i++) {
         vfilt[i] = 0;
         for (size_t j = 0; j < vsize; j++) {
            vfilt[i] +=
               (/*1+*/mu*V[j].range) * exp(-0.5*(sqr(V[i].vertex.x - V[j].vertex.x)
               + sqr(V[i].vertex.y - V[j].vertex.y)) / sigma2) / (2*M_PI*sigma2);
         }
         // thresholding and clustering
         if (vfilt[i] > threshold) {
            // convert to robot's frame of reference
            double dx = V[i].vertex.x - x0;
            double dy = V[i].vertex.y - y0;
            double r = sqrt(dx*dx + dy*dy);
            double dth = atan2(dy, dx)-th0;
            double x = r*cos(dth);
            double y = r*sin(dth);
            CvPoint2D32f pt;
            pt.x = x;
            pt.y = y;
            cvSeqPush(_point32fSeq, &pt);
         }
      }
      // (from OpenCV source code...)
      // the function splits the input sequence or set into one or more equivalence classes.
      // is_equal(a,b,...) returns non-zero if the two sequence elements
      // belong to the same class. the function returns sequence of integers -
      // 0-based class indexes for each element.
      //
      // The algorithm is described in "Introduction to Algorithms"
      // by Cormen, Leiserson and Rivest, chapter "Data structures for disjoint sets"
      CvSeq* labels = 0;
      int classNum = cvSeqPartition(_point32fSeq, 0, &labels, is_equal32f, &clusterThreshold);

      // calculate centre of each cluster
      vector< vector< CvPoint2D32f* > > table(classNum);
      for(int l = 0; l < labels->total; l++)
         table[*(int*)cvGetSeqElem(labels, l)].push_back((CvPoint2D32f*)cvGetSeqElem(_point32fSeq, l));
      for (int c = 0; c < classNum; c++) {
         int pointNum = table[c].size();
         if (pointNum < pointThreshold)
            continue;   // do not even consider clusters too small
         vector< CvPoint2D32f* >::iterator pi, piEnd = table[c].end();
         double xSum = 0, ySum = 0;
         for (pi = table[c].begin(); pi != piEnd; pi++) {
            xSum += (*pi)->x;
            ySum += (*pi)->y;
         }
         targetVec.push_back(point_t(xSum/pointNum, ySum/pointNum));
      }

      // debug image
      if (_debug) {
         int Nc = space_union.getNumOfContours();
         int npts[Nc];
         CvPoint* pts[Nc];
         for (int c = 0; c < Nc; c++) {
            // initialize points arrays from polygon's contours
            npts[c] = space_union.getNumOfVertices(c);
            pts[c] = new CvPoint[npts[c]];
            gpc_vertex* v = space_union.getVertexArray(c);
            for (int i = 0; i < npts[c]; i++) {
               double dx = v[i].x - x0;
               double dy = v[i].y - y0;
               double r = sqrt(dx*dx + dy*dy);
               double dth = atan2(dy, dx)-th0;
               double x = r*cos(dth);
               double y = r*sin(dth);
               pts[c][i].x = METER2PIXEL(y) + TMP_WINDOW_WIDTH/2;
               pts[c][i].y = METER2PIXEL(x) + TMP_WINDOW_HEIGHT/2;
            }
         }
         cvSet(_tmpImg, cvScalar(255,255,255)); // clear debug image
         if (Nc > 0)
            cvFillPoly(_tmpImg, pts, npts, Nc, cvScalar(255, 220, 200));
         // draw violation points
         for (size_t i = 0; i < vsize; i++) {
            double dx = V[i].vertex.x - x0;
            double dy = V[i].vertex.y - y0;
            double r = sqrt(dx*dx + dy*dy);
            double dth = atan2(dy, dx)-th0;
            double x = r*cos(dth);
            double y = r*sin(dth);
            CvPoint p = cvPoint(METER2PIXEL(y) + TMP_WINDOW_WIDTH/2,
                                METER2PIXEL(x) + TMP_WINDOW_HEIGHT/2);
            cvSet2D(_tmpImg, p.y, p.x, cvScalar(255,0,0));
            // filtered point
            if (vfilt[i] > threshold)
               cvCircle(_tmpImg, p, 2, cvScalar(0,0,255));
         }
         // deallocate memory
         for (int c = 0; c < Nc; c++)
            delete[] pts[c];
      }
   }
   // add current polygon to queue
   _Space.push_front(space);
   if (_Space.size() > (size_t)_steps) {
      // remove exceeding elements
      Polygon* last_space = _Space.back();
      delete last_space;   // delete polygon's pointer
      _Space.pop_back();   // and remove it from queue
   }
   
   // finally calculate distance and direction of each horizontal line
   _target.clear();
   vector< point_t >::iterator ti, tiEnd = targetVec.end();
   for (ti = targetVec.begin(); ti != tiEnd; ti++) {
      target_t t;
      t.distance = ti->rad;
      // left PI/2, right -PI/2
      t.bearing = ti->th;
      // no height information of course...
      _target.push_back(t);
   }
   // final number of detected people
   _howMany = _target.size();
   // draw the last things for debugging
   if (_debug) {
      // draw moving points
      for (int i = 0; i < _howMany; i++) {
         CvPoint p = cvPoint(METER2PIXEL(targetVec[i].y) + TMP_WINDOW_WIDTH/2,
            METER2PIXEL(targetVec[i].x) + TMP_WINDOW_HEIGHT/2);
         cvLine(_tmpImg, cvPoint(TMP_WINDOW_WIDTH/2, TMP_WINDOW_HEIGHT/2), p, cvScalar(0,128,255), 2);
      }
      
      cvFlip(_tmpImg, NULL, -1);
      CvMat subImg;
      cvGetSubRect(_tmpImg, &subImg, cvRect(TMP_WINDOW_WIDTH/2 - (int)(DEBUG_WINDOW_WIDTH*DEBUG_SCALE)/2,
                                            TMP_WINDOW_HEIGHT/2 - (int)(DEBUG_WINDOW_HEIGHT*DEBUG_SCALE),
                                            (int)(DEBUG_WINDOW_WIDTH*DEBUG_SCALE),
                                            (int)(DEBUG_WINDOW_HEIGHT*DEBUG_SCALE)));
      cvResize(&subImg, _debugImage, CV_INTER_NN);
      cvShowImage("Motion detector", _debugImage);
      if (_delay)
        cvWaitKey(_delay);  // handles event processing of HIGHGUI library
   }
}


int MotionDetector::getHowMany()
{
   return _howMany;
}


double MotionDetector::getDistance(int personID) throw (MotionDetectorException)
{
   if ((personID >= 0) && (personID < _howMany))
      return _target[personID].distance;
   throw MotionDetectorException("Index out of boundaries in MotionDetector::getDistance(int personID)", EINVAL);
}


double MotionDetector::getBearing(int personID) throw (MotionDetectorException)
{
   if ((personID >= 0) && (personID < _howMany))
      return _target[personID].bearing;
   throw MotionDetectorException("Index out of boundaries in MotionDetector::getDirection(int personID)", EINVAL);
}


int MotionDetector::getClosest()
{
   int personID = -1;
   double minDist = INFINITY;
   int maxId = getHowMany();
   for (int id = 0; id < maxId; id++)
      if (getDistance(id) < minDist)
         minDist = getDistance(personID = id);
   return personID;
}


int MotionDetector::getFarthest()
{
   int personID = -1;
   double maxDist = -INFINITY;
   int maxId = getHowMany();
   for (int id = 0; id < maxId; id++)
      if (getDistance(id) > maxDist)
         maxDist = getDistance(personID = id);
   return personID;
}


void MotionDetector::setDebug(bool enable, int delay)
{
   if (!_debug && (_debug = enable))
   {
      _debugImage = cvCreateImage(cvSize((int)(DEBUG_WINDOW_WIDTH),
                                         (int)(DEBUG_WINDOW_HEIGHT)),
                                         IPL_DEPTH_8U, 3);
      _tmpImg = cvCreateImage(cvSize(TMP_WINDOW_WIDTH, TMP_WINDOW_HEIGHT),
                              IPL_DEPTH_8U, 3);
      _grayImg = cvCreateImage(cvSize(TMP_WINDOW_WIDTH, TMP_WINDOW_HEIGHT),
                               IPL_DEPTH_8U, 1);
      cvNamedWindow("Motion detector", 1);
   }
   _delay = delay;
}


bool MotionDetector::saveSnapshot(const char* filename)
{
   if (_debug && !cvSaveImage(filename, _debugImage))
      return true;
   return false;
}
