/***************************************************************************
 *   Copyright (C) 2011 by Nicola Bellotto                                 *
 *   nbellotto@lincoln.ac.uk                                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef POLYGON_H
#define POLYGON_H

extern "C"
{
#include "gpc.h"
}

/**
@author Nicola Bellotto
*/
class Polygon{
public:
   /**
    * Constructor
    * @param numOfContours Number of contours
    */
   Polygon(int numOfContours = 1);

   /**
    * Constructor from GPC polygon
    * @param gpcPolygon GPC polygon
    */
   Polygon(gpc_polygon* gpcPolygon);

   /**
    * Copy Constructor
    */
   Polygon(const Polygon& polygon);
   
   /**
   * Destructor
   */
   ~Polygon();

   /**
    * Set the contour
    * @param contourNum Contour number
    * @param numOfVertices Number of vertices
    * @param isHole The contour is a hole (@p false by default)
    */
   void setContour(int contourNum, int numOfVertices, bool isHole = false);

   /**
    * Return number of contours
    * @return Number of contours
    */
   int getNumOfContours();

   /**
    * Return the number of vertices of the contour
    * @param contourNum Contour's number
    * @return Number of vertices
    */
   int getNumOfVertices(int contourNum);

   /**
    * Return whether the contour is a hole
    * @param contourNum Contour's number
    * @return @p true if it is a hole, @p false otherwise
    */
   bool isHole(int contourNum);

   /**
    * Return a pointer to the contour's array of vertices
    * @param contourNum Number of contour
    * @return Pointer to array
    */
   gpc_vertex* getVertexArray(int contourNum);

   /**
   * Perform assignment operation
   * @param polygon Origin
   * @return Copy polygon
   */
   Polygon operator= (const Polygon& polygon);

   /**
   * Perform union and assignment operation
   * @param polygon Origin
   * @return Union polygon
   */
   Polygon operator|= (const Polygon& polygon);

   /**
   * Perform union operation
   * @param polygon_1 1st operand
   * @param polygon_2 2nd operand
   * @return Union polygon
   */
   friend Polygon operator| (const Polygon& polygon_1, const Polygon& polygon_2);

   /**
   * Perform intersection and assignment operation
   * @param polygon Origin
   * @return Intersection polygon
   */
   Polygon operator&= (const Polygon& polygon);

   /**
   * Perform intersection operation
   * @param polygon_1 1st operand
   * @param polygon_2 2nd operand
   * @return Intersection polygon
   */
   friend Polygon operator& (const Polygon& polygon_1, const Polygon& polygon_2);
   
   /**
   * Perform difference and assignment operation
   * @param polygon Origin
   * @return Difference polygon
   */
   Polygon operator-= (const Polygon& polygon);

   /**
   * Perform difference operation
   * @param polygon_1 1st operand
   * @param polygon_2 2nd operand
   * @return Difference polygon
   */
   friend Polygon operator- (const Polygon& polygon_1, const Polygon& polygon_2);
   
   bool isInside(const gpc_vertex& p);

private:
   gpc_polygon* Poly;
   int NumOfContours;
};

Polygon operator| (const Polygon& polygon_1, const Polygon& polygon_2);

Polygon operator& (const Polygon& polygon_1, const Polygon& polygon_2);

Polygon operator- (const Polygon& polygon_1, const Polygon& polygon_2);

#endif
