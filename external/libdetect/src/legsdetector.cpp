/***************************************************************************
 *   Copyright (C) 2011 by Nicola Bellotto                                 *
 *   nbellotto@lincoln.ac.uk                                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "legsdetector.h"

#include <iostream>
#include <errno.h>
#include <vector>

// parameters for old update (deprecated)
#define FILTER_SIZE 0//2                  // size of smoothing filter
#define MIN_CONSECUTIVE 4//1              // minimum number of consecutive adjacent points forming a min or max
#define MAXIMUM_RANGE   8              // maximum detection range, in [m]
#define NULL_DERIVATIVE_THRESHOLD 5e-2 // for derivative calculus, in [m]
#define IN_LEGS_DEPTH   0.3//0.2            // minimum inner legs gap, in [m]
#define OUT_LEGS_DEPTH  0.3//0.2            // minimum outer legs gap, in [m]
#define MAX_LEGS_STEP   0.5            // maximum step, in [m]

#define NEW_MAXIMUM_RANGE  8                    // maximum detection range, in [m]
#define DELTA(k, max) ((int)(k*max/180. + .5))  // minimization filter (k = bound [deg], max = aperture [deg])

#define GRID_RESOLUTION 0.02    // [m]
#define METER2PIXEL(m) ((int)((float)(m) / (GRID_RESOLUTION) + 0.5))
#define PIXEL2METER(p) ((float)(p) * (GRID_RESOLUTION))
#define DEBUG_WINDOW_WIDTH 640//(2 * METER2PIXEL(NEW_MAXIMUM_RANGE))
#define DEBUG_WINDOW_HEIGHT 480//(METER2PIXEL(NEW_MAXIMUM_RANGE))
#define DEBUG_SCALE 1   // debug image size scaling
// the debug map will be rotated of -90 before visualization
#define DEBUG_MAP_WIDTH (DEBUG_WINDOW_HEIGHT * DEBUG_SCALE)//(2 * METER2PIXEL(NEW_MAXIMUM_RANGE))
#define DEBUG_MAP_HEIGHT (DEBUG_WINDOW_WIDTH * DEBUG_SCALE)//(METER2PIXEL(NEW_MAXIMUM_RANGE))

// parameters of update
#define MIN_LONG_EDGE   0.30  // [m] basically, minimum distance from obstacles
#define MIN_SHORT_EDGE  0.10  // [m] basically, minimum step length
#define MIN_EDGE_DIST   0.10  // [m] minimum distance between two right or two left vertical edges
#define MAX_LEG_WIDTH   0.20
#define MIN_LEG_WIDTH   0.10
#define MAX_LEGS_GAP    0.35
// #define MAX_RANGE_DIFF  0.10  // [m] (maximum) horizontal edge gradient
#define MAX_GRADIENT    0.50  // sin(pi/6)
// #define MAX_GRADIENT    0.707 // sin(pi/4)

    
LegsDetector::LegsDetector(int bufferLength, uint selectivity, double filter)
{
   _bufferLength = bufferLength;
   _delta = DELTA(filter, _bufferLength);

   _selectivity = selectivity;

   _howMany = 0;

   _debug = false;
}


LegsDetector::~LegsDetector()
{
   if (_debug)
   {
      cvReleaseImage(&_mapImg);
      cvReleaseImage(&_debugImage);
      cvReleaseImage(&_tmpImg);
      cvReleaseImage(&_tmpImgT);
      cvDestroyWindow("Legs detector");
//       cvDestroyWindow("Old legs detector");
   }
}

   
void LegsDetector::oldUpdate(const std::vector< laser_t >& laserBuffer, bool findOneLeg, bool findAdjacent)
{
   double d[_bufferLength];
   vector<int> vfirst;
   vector<int> vmiddle;
   vector<int> vpair;
   vector<int> vsingle;

   // filter laser data to make it smoother
   for (int i = 0; i < _bufferLength; i++)
   {
      d[i] = 0;
      for (int j = (i < FILTER_SIZE) ? -i : -FILTER_SIZE;
           j <= (i < _bufferLength - FILTER_SIZE ? FILTER_SIZE : _bufferLength - i - 1);
           j++)
         d[i] += laserBuffer[i+j].range;
      if ((d[i] /= 2*FILTER_SIZE + 1.0) > MAXIMUM_RANGE)
        d[i] = MAXIMUM_RANGE;
   }

   // detect min and max points
   bool end = false;
   int status = 0;
   int segNum;          // number of detected segments
   int consecutive = 0; // number of consecutive adjacent points
   for (int i = 1; i < _bufferLength; i++)
   {
      // determine possible min or max segment (null derivative)
      if ((fabs(d[i] - d[i-1]) < NULL_DERIVATIVE_THRESHOLD) && (i < (_bufferLength - 1))) {
         consecutive++;
         if (!end)
            // store first index of the segment
            vfirst.push_back(i-1);
         end = true;
      }
      else if (end && (consecutive >= MIN_CONSECUTIVE)) {
         // store the index in the middle of the segment
         vmiddle.push_back((int)(vfirst[vfirst.size() - 1] + i
                                                           - 1   /* coz we're over the end */
                                                           + 0.5 /* to round */
                                                           ) / 2);
         end = false;

         // determine sequence max->min->max->min->max for possible legs
         if ((segNum = vmiddle.size()) > 1)  // number of current segments
         {
            switch (status)
            {
               case 1:  // look for max (between legs)
                  if (d[vmiddle[segNum - 1]] > d[vmiddle[segNum - 2]] + IN_LEGS_DEPTH)
                  {
                     status++;
                     break;
                  }
                  status = 0;
                  break;
   
               case 2:  // look for min (second leg)
                  if (d[vmiddle[segNum - 1]] < d[vmiddle[segNum - 2]] - IN_LEGS_DEPTH) {
                     status++;
                     break;
                  }
                  else if (findOneLeg){
                     // store the point in the middle of the sequence (leg minimum)
                     vsingle.push_back(segNum - 3);
                  }
                  status = 0;
                  break;
   
               case 3:  // look for max (end)
                  if (d[vmiddle[segNum - 1]] > d[vmiddle[segNum - 2]] + OUT_LEGS_DEPTH)
                  {
                     // store the point in the middle of the sequence (middle of the legs)
                     vpair.push_back(segNum - 3);
                     if (findAdjacent) {
                        status = 2;
                     }
                     else {
                        status = 0;
                     }
                     break;
                  }
                  status = 0;
                  break;
            }
            // if no one of the above cases, then look for min (first leg)
            if ((!status) && (d[vmiddle[segNum - 1]] < d[vmiddle[segNum - 2]] - OUT_LEGS_DEPTH))
               status++;
         }
         consecutive = 0;
      }
      else if (end) {
         consecutive = 0;
      }
   }

   _target.clear();
   // possible legs pair detected
   int found = vpair.size();
   if (found)
   {
      for (int ti = 0; ti < found; ti++)
      {
         double alpha = (vmiddle[vpair[ti] + 1] - vmiddle[vpair[ti] - 1]) *
                        M_PI / (_bufferLength - 1);   // angle between legs
         double d1 = d[vmiddle[vpair[ti] - 1]];      // distance left leg
         double d2 = d[vmiddle[vpair[ti] + 1]];      // distance right leg
         double step = sqrt((d1 * sin(alpha)) * (d1 * sin(alpha)) +
                            (d2 - d1 * cos(alpha)) * (d2 - d1 * cos(alpha))); // aperture between legs
         // consider only legs not too close or too far from each other and with a limited step length
         if (step < MAX_LEGS_STEP)
         {
            target_t t;
            // the distance is an average between the two leg distances
            t.distance = (d1 + d2) / 2.0;
            // left PI/2, right -PI/2
            t.bearing = laserBuffer[vmiddle[vpair[ti]]].angle;
            // no height information of course...
            t.pattern = 'U';
            _target.push_back(t);
         }
      }
   }
   // possible single legs detected
   found = vsingle.size();
   if (found) {
      for (int ti = 0; ti < found; ti++) {
         // consider only legs not too close or too far from each other and with a limited step length
         target_t t;
         // the distance is an average between the two leg distances
         t.distance = d[vmiddle[vsingle[ti]]];
         // left PI/2, right -PI/2
         t.bearing = laserBuffer[vmiddle[vsingle[ti]]].angle;
         // no height information of course...
         t.pattern = 'O';
         _target.push_back(t);
      }
   }
   // final number of detected people
   _howMany = _target.size();
   
   if (_debug)
   {
      // clear image
      cvSet(_mapImg, CV_RGB(235, 235, 235));
      // axes
      cvLine(_mapImg, cvPoint(120, 590), cvPoint(480, 590), cvScalar(128, 128, 128), 1);
      cvLine(_mapImg, cvPoint(120, 590), cvPoint(120, 320), cvScalar(128, 128, 128), 1);
      FILE* rawdata = fopen("raw.dat", "w+");
      FILE* filtdata = fopen("filt.dat", "w+");
      fprintf(rawdata, "0\t%.3f\n", laserBuffer[0].range);
      fprintf(filtdata, "0\t%.3f\n", d[0]);
      for (int i = 1; i < _bufferLength; i++)
      {
         fprintf(rawdata, "%d\t%.3f\n", i, laserBuffer[i].range);
         fprintf(filtdata, "%d\t%.3f\n", i, d[i]);
         // draw laser data
         int x1 = -(int)(laserBuffer[i-1].x * 30 + 0.5) + 300;
         int y1 = -(int)(laserBuffer[i-1].y * 30 + 0.5) + 300;
         int x2 = -(int)(laserBuffer[i].x * 30 + 0.5) + 300;
         int y2 = -(int)(laserBuffer[i].y * 30 + 0.5) + 300;
         cvLine(_mapImg,
               cvPoint(y1, x1),
               cvPoint(y2, x2),
               cvScalar(0, 0, 0));
   
         // draw filtered laser data
         x1 = -(int)(d[i-1] * sin(laserBuffer[i-1].angle) * 30 + 0.5) + 300;
         y1 = -(int)(d[i-1] * cos(laserBuffer[i-1].angle) * 30 + 0.5) + 300;
         x2 = -(int)(d[i] * sin(laserBuffer[i].angle) * 30 + 0.5) + 300;
         y2 = -(int)(d[i] * cos(laserBuffer[i].angle) * 30 + 0.5) + 300;
         cvLine(_mapImg,
               cvPoint(x1, y1),
               cvPoint(x2, y2),
               cvScalar(0, 200, 0));
   
         // draw filtered laser data on XY graph (X -> angle, Y -> range)
         x1 = - (int)((i - 1) * (360.0 / _bufferLength) + 0.5) + 360 + 120;
         y1 = 590 - (int)(d[i-1] * 30);
         x2 = - (int)(i * (360.0 / _bufferLength) + 0.5) + 360 + 120;
         y2 = 590 - (int)(d[i] * 30 + 0.5);
         cvLine(_mapImg,
               cvPoint(x1, y1),
               cvPoint(x2, y2),
               cvScalar(0, 0, 200));
   
         // draw middle points
         for (uint i = 0; i < vmiddle.size(); i++)
         {
            int xmin = (int)(-vmiddle[i] * (360.0 / _bufferLength) + 0.5) + 360 + 120;
            int ymin = 590 - (int)(d[vmiddle[i]] * 30 + 0.5);
            cvLine(_mapImg,
                  cvPoint(xmin - 5, ymin),
                  cvPoint(xmin + 5, ymin),
                  cvScalar(0, 200, 0));
            cvLine(_mapImg,
                  cvPoint(xmin, ymin - 5),
                  cvPoint(xmin, ymin + 5),
                  cvScalar(0, 200, 0));
         }
         
         // draw pointer to legs
         for (int i = 0; i < _howMany; i++)
         {
            int x = -(int)(_target[i].distance * cos(-_target[i].bearing + M_PI / 2) * 30 + 0.5) + 300;
            int y = -(int)(_target[i].distance * sin(-_target[i].bearing + M_PI / 2) * 30 + 0.5) + 300;
            cvLine(_mapImg,
                  cvPoint(300, 300),
                  cvPoint(x, y),
                  cvScalar(200, 0, 0));
            // in the XY graph      
            x = (int)((-_target[i].bearing * 180 / M_PI + 90) * 2 + 0.5) + 120;
            cvLine(_mapImg,
                  cvPoint(x, 590),
                  cvPoint(x, 350),
                  cvScalar(200, 0, 0));
         }
      }
      fclose(rawdata);
      fclose(filtdata);
//       cvShowImage("Old legs detector", _mapImg);
   //   char fname[64];
   //   sprintf(fname, "debug_legs_%d.png", debugStep);
   //   cvSaveImage(fname, _mapImg);
      if (_delay)
        cvWaitKey(_delay);  // handles event processing of HIGHGUI library
   }
}


int LegsDetector::getHowMany()
{
   return _howMany;
}


double LegsDetector::getDistance(int personID) throw (LegsDetectorException)
{
   if ((personID >= 0) && (personID < _howMany))
      return _target[personID].distance;
   throw LegsDetectorException("Index out of boundaries in LegsDetector::getDistance(int personID)", EINVAL);
}


double LegsDetector::getBearing(int personID) throw (LegsDetectorException)
{
   if ((personID >= 0) && (personID < _howMany))
      return _target[personID].bearing;
   throw LegsDetectorException("Index out of boundaries in LegsDetector::getDirection(int personID)", EINVAL);
}


int LegsDetector::getClosest()
{
   int personID = -1;
   double minDist = INFINITY;
   int maxId = getHowMany();
   for (int id = 0; id < maxId; id++)
      if (getDistance(id) < minDist)
         minDist = getDistance(personID = id);
   return personID;
}


int LegsDetector::getFarthest()
{
   int personID = -1;
   double maxDist = -INFINITY;
   int maxId = getHowMany();
   for (int id = 0; id < maxId; id++)
      if (getDistance(id) > maxDist)
         maxDist = getDistance(personID = id);
   return personID;
}


void LegsDetector::setDebug(bool enable, int delay)
{
   if (!_debug && (_debug = enable))
   {
      _mapImg =     cvCreateImage(cvSize(600, 600), IPL_DEPTH_8U, 3);
      _debugImage = cvCreateImage(cvSize(DEBUG_WINDOW_WIDTH, DEBUG_WINDOW_HEIGHT), IPL_DEPTH_8U, 3);
      _tmpImg =     cvCreateImage(cvSize(DEBUG_MAP_WIDTH, DEBUG_MAP_HEIGHT), IPL_DEPTH_8U, 3);
      _tmpImgT =    cvCreateImage(cvSize(DEBUG_MAP_HEIGHT, DEBUG_MAP_WIDTH), IPL_DEPTH_8U, 3);
      cvNamedWindow("Legs detector", 1);
//       cvNamedWindow("Old legs detector", 1);
   }
   _delay = delay;
}


void LegsDetector::update(const std::vector< laser_t >& laserBuffer)
{
   // 3 stage processing
   // first remove high peaks due to absorving materials
   laser_t first_laser[_bufferLength];
   for (int i = 0; i < _bufferLength; i++)
   {
      first_laser[i].range = DBL_MAX;
      double angle = first_laser[i].angle = laserBuffer[i].angle;
      for (int k = std::max(0, i-_delta); k <= std::min(_bufferLength-1, i+_delta); k++)
      {
         double range;
         if (laserBuffer[k].range < first_laser[i].range)
         {
            range = first_laser[i].range = laserBuffer[k].range;
            first_laser[i].x = range * cos(angle);
            first_laser[i].y = range * sin(angle);
//             laser[i].x = laserBuffer[k].x;   // buggy code, but works ;)
//             laser[i].y = laserBuffer[k].y;   // buggy code, but works ;)
         }
      }
   }
   // then removes peaks due to very thin objects
   laser_t second_laser[_bufferLength];
   for (int i = 0; i < _bufferLength; i++)
   {
      second_laser[i].range = 0;
      double angle = second_laser[i].angle = first_laser[i].angle;
      for (int k = std::max(0, i-2*_delta); k <= std::min(_bufferLength-1, i+2*_delta); k++)
      {
         if (first_laser[k].range > second_laser[i].range)
         {
            double range = second_laser[i].range = first_laser[k].range;
            second_laser[i].x = range * cos(angle);
            second_laser[i].y = range * sin(angle);
         }
      }
   }
   // finally, brings back objects to original size
   laser_t laser[_bufferLength];
   for (int i = 0; i < _bufferLength; i++)
   {
      laser[i].range = DBL_MAX;
      double angle = laser[i].angle = second_laser[i].angle;
      for (int k = std::max(0, i-_delta); k <= std::min(_bufferLength-1, i+_delta); k++)
      {
         if (second_laser[k].range < laser[i].range)
         {
            double range = laser[i].range = second_laser[k].range;
            laser[i].x = range * cos(angle);
            laser[i].y = range * sin(angle);
         }
      }
   }

   // start extracting all the vertical edges of interest
   // remembering the scan goes from right (-PI/2) to left (+PI/2)
   // left and right edges correspond to the robot's point of view
   //
   //                  (p2)             (p2)
   //                   |    (p2)-(p2)   |
   //                   |     |    |     |
   //                   |     |   l|     |r
   //                   |     |    |     |
   //                  L|     |R  (p1)--(p1)
   //                   |     |
   //                   |     |
   //                  (p1)--(p1)
   //             
   vector< edge_t<point_t> > vEdge;
   double prevRange = laser[0].range;
   for (int id = 1; id < _bufferLength; id++)
   {
      double range = laser[id].range;
      
      if ((range - prevRange) > MIN_LONG_EDGE)      // possible left long edge
      {
         edge_t<point_t> e = {Point(laser[id-1].x, laser[id-1].y),
                              Point(laser[id].x, laser[id].y), 'L'};
         vEdge.push_back(e);
      }
      else if ((prevRange - range) > MIN_LONG_EDGE) // possible right long edge
      {
         edge_t<point_t> e = {Point(laser[id].x, laser[id].y),
                              Point(laser[id-1].x, laser[id-1].y), 'R'};
         vEdge.push_back(e);
      }
      else if ((range - prevRange) > MIN_SHORT_EDGE) // possible left short edge
      {
         edge_t<point_t> e = {Point(laser[id-1].x, laser[id-1].y),
                              Point(laser[id].x, laser[id].y), 'l'};
         vEdge.push_back(e);
      }
      else if ((prevRange - range) > MIN_SHORT_EDGE) // possible right short edge
      {
         edge_t<point_t> e = {Point(laser[id].x, laser[id].y),
                              Point(laser[id-1].x, laser[id-1].y), 'r'};
         vEdge.push_back(e);
      }

      prevRange = range;
   }
   // remove edges too close to each other
   if (!vEdge.empty())
   {
      vector<edge_t<point_t> >::iterator first = vEdge.begin();
      vector<edge_t<point_t> >::iterator second = first + 1;
      while (second != vEdge.end())
      {
         char t1 = toupper(first->type);
         char t2 = toupper(second->type);
         if (((t1 == 'R') && (t2 == 'R') &&
             (getDistance(second->p2, first->p1) < MIN_EDGE_DIST)))
         {
            first->p1 = second->p1;
            first->type = 'R';
            second = vEdge.erase(second);
         }
         else if (((t1 == 'L') && (t2 == 'L') &&
                  (getDistance(first->p2, second->p1) < MIN_EDGE_DIST)))
         {
            first->p2 = second->p2;
            first->type = 'L';
            second = vEdge.erase(second);
         }
         else
         {
            first++;
            second++;
         }
      }
   }
   // draw some stuff for debugging...
   // (must be done now, before vEdge is modified)
   if (_debug)
   {
      cvSet(_tmpImg, cvScalar(255,255,255));
      CvPoint start = cvPoint(METER2PIXEL(laser[0].y) + DEBUG_MAP_WIDTH/2,
                              METER2PIXEL(laser[0].x) + DEBUG_MAP_HEIGHT/2);
      // draw the laser data
      for (int i = 1; i < _bufferLength; i++)
      {
         CvPoint end = cvPoint(METER2PIXEL(laser[i].y) + DEBUG_MAP_WIDTH/2,
                               METER2PIXEL(laser[i].x) + DEBUG_MAP_HEIGHT/2);
         cvLine(_tmpImg, start, end, cvScalar(0,0,0));
         start = end;
      }
      // draw the extremes
      for (uint i = 0; i < vEdge.size(); i++)
      {
         CvScalar color;
         switch (vEdge[i].type)
         {
            case 'R':
               color = cvScalar(0,0,255); // red
               break;
            case 'L':
               color = cvScalar(255,0,0); // blue
               break;
            case 'r':
               color = cvScalar(0,196,255);  // yellow
               break;
            case 'l':
               color = cvScalar(64,255,0);  // green
               break;
         }
         // draw min extremes
         CvPoint center = cvPoint(METER2PIXEL(vEdge[i].p1.y) + DEBUG_MAP_WIDTH/2,
                                  METER2PIXEL(vEdge[i].p1.x) + DEBUG_MAP_HEIGHT/2);
         cvCircle(_tmpImg, center, 3, color);
         // draw max extremes
         CvPoint c1 = cvPoint(METER2PIXEL(vEdge[i].p2.y) - 3 + DEBUG_MAP_WIDTH/2,
                              METER2PIXEL(vEdge[i].p2.x) - 3 + DEBUG_MAP_HEIGHT/2);
         CvPoint c2 = cvPoint(METER2PIXEL(vEdge[i].p2.y) + 3 + DEBUG_MAP_WIDTH/2,
                              METER2PIXEL(vEdge[i].p2.x) + 3 + DEBUG_MAP_HEIGHT/2);
         cvRectangle(_tmpImg, c1, c2, color);
      }
   }
   // extract the horizontal lines of interest
   vector< edge_t<point_t> > hEdge;
   getUpattern(vEdge, hEdge);
   if (_selectivity < 2)
      getPpattern(vEdge, hEdge);
   if (_selectivity < 1)
      getOpattern(vEdge, hEdge);
   // finally calculate distance and direction of each horizontal line
   _target.clear();
   vector< edge_t<point_t> >::iterator itend = hEdge.end();
   for (vector< edge_t<point_t> >::iterator it = hEdge.begin(); it != itend; it++)
   {
      target_t t;
      // the distance is an average between the two points
      double xm = ((it->p1).x + (it->p2).x) / 2;
      double ym = ((it->p1).y + (it->p2).y) / 2;
      t.distance = sqrt(sqr(xm) + sqr(ym));
      // left PI/2, right -PI/2
      t.bearing = atan2(ym, xm);
      // no height information of course...
      t.pattern = it->type;
      _target.push_back(t);
   }
   // final number of detected people
   _howMany = _target.size();
   // draw the last things for debugging
   if (_debug)
   {
      // draw horizontal edges
      for (uint i = 0; i < hEdge.size(); i++)
      {
         CvPoint p1 = cvPoint(METER2PIXEL(hEdge[i].p1.y) + DEBUG_MAP_WIDTH/2,
                              METER2PIXEL(hEdge[i].p1.x) + DEBUG_MAP_HEIGHT/2);
         CvPoint p2 = cvPoint(METER2PIXEL(hEdge[i].p2.y) + DEBUG_MAP_WIDTH/2,
                              METER2PIXEL(hEdge[i].p2.x) + DEBUG_MAP_HEIGHT/2);
//          cvLine(_tmpImg, p1, p2, cvScalar(0,128,255), 2);
         CvPoint pm = cvPoint((p1.x + p2.x) / 2, (p1.y + p2.y) / 2);
         int thick;
         if (hEdge[i].type == 'U')
            thick = 3;
         else if (hEdge[i].type == 'P')
            thick = 2;
         else
            thick = 1;
         cvLine(_tmpImg, cvPoint(DEBUG_MAP_WIDTH/2, DEBUG_MAP_HEIGHT/2), pm, cvScalar(0,128,255), thick);
      }
      
//       cvFlip(_tmpImg, NULL, -1);
      cvTranspose(_tmpImg, _tmpImgT);
      cvFlip(_tmpImgT, _tmpImgT, 0);
      cvResize(_tmpImgT, _debugImage, CV_INTER_NN);
      cvShowImage("Legs detector", _debugImage);
      if (_delay)
        cvWaitKey(_delay);  // handles event processing of HIGHGUI library
   }
}


// get all the 'U' patterns
//
//   |  ||  |       |  ||  |
//   |  ||  |       |  ||  |
//   |  ||  |  and  |  |+--+
//   |  ||  |       |  |
//   +--++--+       +--+
//
void LegsDetector::getUpattern(vector< edge_t<point_t> >& input,
                               vector< edge_t<point_t> >& output)
{
   vector< edge_t<point_t> >::iterator first = input.begin();
   while (first != input.end())
   {
      // look for the right edge
      if (first->type == 'R')
      {
         vector< edge_t<point_t> >::iterator second = first + 1;
         double d;
         if ((second != input.end()) && (second->type == 'L') &&
//              (fabs(second->p1.r - first->p1.r) < MAX_RANGE_DIFF) &&
             ((d = dist(second->p1, first->p1)) < MAX_LEG_WIDTH) &&
             (fabs(second->p1.r - first->p1.r)/d < MAX_GRADIENT) &&
             (d > MIN_LEG_WIDTH))
         {  // first leg detected, look for the second
            vector< edge_t<point_t> >::iterator third = second + 1;
            bool found = false;
            while (third != input.end())
            {
               // look for the right edge of the second leg
               if ((third->type == 'R') &&
                   ((d = dist(third->p1, second->p1)) < MAX_LEGS_GAP))
               {
                  vector< edge_t<point_t> >::iterator forth = third + 1;
                  if ((forth != input.end()) && (forth->type == 'L') &&
//                       (fabs(forth->p1.r - third->p1.r) < MAX_RANGE_DIFF) &&
                      ((d = dist(forth->p1, third->p1)) < MAX_LEG_WIDTH) &&
                      (fabs(forth->p1.r - third->p1.r)/d < MAX_GRADIENT) &&
                      (d > MIN_LEG_WIDTH))
                  {
                     // add new horizontal edge
                     edge_t<point_t> e = {first->p1, forth->p1, 'U'};
                     output.push_back(e);
                     // remove current legs edges
                     input.erase(forth);
                     input.erase(third);
                     input.erase(second);
                     first = input.erase(first);
                     found = true;
                     break;
                  }
                  else
                     third++;
               }
               else
                  third++;
            }
            // if second leg not found, go ahead
            if (!found)
               first++;
         }
         else
            first++;
      }
      else
         first++;
   }
}


// get all the 'P' patterns
//
//   |     |       |     |
//   |     |       |     |
//   |  |--+  and  +--|  |
//   |  |             |  |
//   +--+             +--+
//             
void LegsDetector::getPpattern(vector< edge_t<point_t> >& input,
                               vector< edge_t<point_t> >& output)
{
   vector< edge_t<point_t> >::iterator first = input.begin();
   while (first != input.end())
   {
      // look for the right edge
      if (first->type == 'R')
      {
         // look for the short edge (left or right)
         vector< edge_t<point_t> >::iterator second = first + 1;
         double d;
         char t = toupper(second->type);
         if ((second != input.end()) &&
             ((d = dist(second->p2, second->p1)) < (MAX_LEGS_GAP/* + MAX_LEG_WIDTH*/)))
         {
            switch (t)
            {
               case 'R':
               if (/*(fabs(second->p2.r - first->p1.r) < MAX_RANGE_DIFF) &&*/
                   ((d = dist(second->p2, first->p1)) < MAX_LEG_WIDTH) &&
                   (fabs(second->p2.r - first->p1.r)/d < MAX_GRADIENT) &&
                   (d > MIN_LEG_WIDTH))
               {  // case with right leg ahead
                  // look for the final left edge
                  vector< edge_t<point_t> >::iterator third = second + 1;
                  if ((third != input.end()) && (third->type == 'L') &&
//                       (fabs(third->p1.r - second->p1.r) < MAX_RANGE_DIFF) &&
                      ((d = dist(third->p1, second->p1)) < MAX_LEG_WIDTH) &&
                      (fabs(third->p1.r - second->p1.r)/d < MAX_GRADIENT) &&
                      (d > MIN_LEG_WIDTH))
                  {
                     // add new horizontal edge (well, not really horizontal...)
                     edge_t<point_t> e = {first->p1, third->p1, 'P'};
                     output.push_back(e);
                     // remove current pattern edges
                     input.erase(third);
                     input.erase(second);
                     first = input.erase(first);
                     break;
                  }
               }
               
               case 'L':
               if (/*(fabs(second->p1.r - first->p1.r) < MAX_RANGE_DIFF) &&*/
                   ((d = dist(second->p1, first->p1)) < MAX_LEG_WIDTH) &&
                   (fabs(second->p1.r - first->p1.r)/d < MAX_GRADIENT) &&
                   (d > MIN_LEG_WIDTH))
               {  // case with left leg ahead
                  // look for the final left edge
                  vector< edge_t<point_t> >::iterator third = second + 1;
                  if ((third != input.end()) && (third->type == 'L') &&
//                       (fabs(third->p1.r - second->p2.r) < MAX_RANGE_DIFF) &&
                      ((d = dist(third->p1, second->p2)) < MAX_LEG_WIDTH) &&
                      (fabs(third->p1.r - second->p2.r)/d < MAX_GRADIENT) &&
                      (d > MIN_LEG_WIDTH))
                  {
                     // add new horizontal edge (well, not really horizontal...)
                     edge_t<point_t> e = {first->p1, third->p1, 'P'};
                     output.push_back(e);
                     // remove current pattern edges
                     input.erase(third);
                     input.erase(second);
                     first = input.erase(first);
                     break;
                  }
               }
               
               default:
               first++;
            }
         }
         else
            first++;
      }
      else
         first++;
   }
}


// get all the 'O' patterns
//
//   |      |
//   |      |
//   |      |
//   |      |
//   +------+
//
void LegsDetector::getOpattern(vector< edge_t<point_t> >& input,
                               vector< edge_t<point_t> >& output)
{
   vector< edge_t<point_t> >::iterator first = input.begin();
   while (first != input.end())
   {
      // look for the right edge
      if (first->type == 'R')
      {
         vector< edge_t<point_t> >::iterator second = first + 1;
         double d;
         if ((second != input.end()) && (second->type == 'L') &&
//              (fabs(second->p1.r - first->p1.r) < MAX_RANGE_DIFF) &&
             ((d = dist(second->p1, first->p1)) < (2*MAX_LEG_WIDTH/* + MIN_LEG_WIDTH*/)) &&
             (fabs(second->p1.r - first->p1.r)/d < MAX_GRADIENT) &&
             (d > MIN_LEG_WIDTH))
         {
            // add new horizontal edge
            edge_t<point_t> e = {first->p1, second->p1, 'O'};
            output.push_back(e);
            // remove current left and right edges
            input.erase(second);
            first = input.erase(first);
         }
         else
            first++;
      }
      else
         first++;
   }
}


char LegsDetector::getPattern(int personID) throw (LegsDetectorException)
{
   if ((personID >= 0) && (personID < _howMany))
      return _target[personID].pattern;
   throw LegsDetectorException("Index out of boundaries in LegsDetector::getPattern(int personID)", EINVAL);
}


bool LegsDetector::saveSnapshot(const char* filename)
{
   if (_debug && !cvSaveImage(filename, _debugImage))
      return true;
   return false;
}


bool LegsDetector::saveOldSnapshot(const char* filename)
{
   if (_debug && !cvSaveImage(filename, _mapImg))
      return true;
   return false;
}
