/***************************************************************************
 *   Copyright (C) 2011 by Nicola Bellotto                                 *
 *   nbellotto@lincoln.ac.uk                                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef MOTIONDETECTOR_H
#define MOTIONDETECTOR_H

// #include <CGAL/Cartesian.h>
// #include <CGAL/Polygon_2.h>
// #include <CGAL/Polygon_with_holes_2.h>
// #include <CGAL/Polygon_set_2.h>
// #include <CGAL/Boolean_set_operations_2.h>
// #include <CGAL/minkowski_sum_2.h>


#include "detector.h"
#include "utility.h"
extern "C"
{
#include "gpc.h"
}
#include "polygon.h"

#include <string>
#include <vector>
#include <deque>
#include <math.h>

#include "opencv/cv.h"
#include "opencv/highgui.h"


// typedef CGAL::Cartesian<double>  Kernel;
// typedef Kernel::Point_2          Point_2;
// typedef CGAL::Polygon_2<Kernel>  Polygon_2;
// typedef CGAL::Polygon_with_holes_2<Kernel> Polygonwh_2;
// typedef CGAL::Polygon_set_2<Kernel> Polygon_set_2;


using std::vector;
using std::deque;
using namespace std;

/**
* Laser-based motion detector
*
* @author Nicola Bellotto
*/


class MotionDetectorException : public DetectorException {
protected:
  std::string _errmsg;
  int _errno;
public:

  /**
   * Constructor
   * @param message Exception message
   * @param error Error number
   */
  MotionDetectorException(std::string message = "General exception", int error = 0)
    { _errmsg =  "<MotionDetectorException>: " + message; _errno = error; }
  
  /**
   * Return the exception message
   * @return Exception message
   */
  std::string getMessage() { return _errmsg; }
  
  /**
   * Return the error number
   * @return Error number
   */
  int getError() { return _errno; }
};
  


class MotionDetector : public Detector
{
   typedef struct
   {
      double distance;  // [m]
      double bearing;   // [rad]
   } target_t;

   class point_t {
   public:
      point_t(double x, double y, double rad, double th)
         { this->x = x; this->y = y; this->rad = rad; this->th = th; }
      point_t(double a, double b, bool cart = true)
         {  if (cart)
            { x = a; y = b; rad = sqrt(x*x + y*y); th = atan2(y, x); }
            else
            { x = a * cos(b); y = a * sin(b); rad = a; th = b; }
         }
   public:
      double x;
      double y;
      double rad;
      double th;
   };
   inline point_t Point(double x, double y) { point_t p(x, y); return p; }
   
   typedef struct {
      gpc_vertex vertex;
      double range;
   } reading_t;
   
   template <typename Point>
   struct edge_t { Point p1; Point p2; char type; };

   inline double dist(const point_t& p1, const point_t& p2)
      { return sqrt(sqr(p1.x - p2.x) + sqr(p1.y - p2.y)); }

public:
   /**
   * Constructor
   * @param laserBuffer Buffer of laser readings
   * @param bufferLength Length of the buffer
   * @param steps Number of previous steps to take into account
   * @param steps Maximum detection distance (default 5.0m)
   */
   MotionDetector(int bufferLength, int steps, double maxDistance = 5.0);

   /**
   * Destructor
   * @return 
   */
   ~MotionDetector();

   /**
    * Perform a new detection on the current laser data
    * @param laserBuffer Vector of laser scans
    * @param x0 Observer's X position
    * @param y0 Observer's Y position
    * @param th0 Observer's orientation
    */
   void updateNew(const std::vector< laser_t >& laserBuffer,
               double x0, double y0, double th0);
   
   /**
    * Perform a new detection on the current laser data
    * @param laserBuffer Vector of laser scans
    * @param x0 Observer's X position
    * @param y0 Observer's Y position
    * @param th0 Observer's orientation
    */
   void update(const std::vector< laser_t >& laserBuffer,
               double x0, double y0, double th0);

   /**
    * Perform a new detection on the current laser data
    * @param laserBuffer Vector of laser scans
    */
   void update(const std::vector< laser_t >& laserBuffer)
      { update(laserBuffer, 0, 0, 0); }
   
   /**
    * Reset detector
    */
   void reset() {
      _howMany = 0;
      _target.clear();
   }
   
   /**
    * Returns the number of detected persons (legs pairs)
    * @return Number of persons
    */
   int getHowMany();
    
   /**
    * Get the distance of a particular person (legs pair)
    * @param personID Person ID number, between "0" and "N = getHowMany()"
    * @return Distance in [m]
    */
   double getDistance(int personID) throw (MotionDetectorException);
    
   /**
    * Get the direction of a particular person (legs pair)
    * @param personID Person ID number, between "0" and "N = getHowMany()"
    * @return Bearing in [rad], +PI on the left and -PI on the right
    */
   double getBearing(int personID) throw (MotionDetectorException);

   /**
    * DEPRECATED - Sunstituted by @p getBearing
    */
   double getDirection(int personID) throw (MotionDetectorException) { return getBearing(personID); }
   
   /**
    * Get the legs pattern (only 'P' type available) of a particular person.
    * Only for compatibility with legs detector class.
    * @param personID Person ID number, between "0" and "N = getHowMany()"
    * @return Pattern type
    */
   char getPattern(int personID) throw (MotionDetectorException) { return 'P'; }

   /**
    * Get the ID of the closest person
    * @return Person ID
    */
   int getClosest();
   
   /**
    * Get the ID of the farthest person
    * @return Person ID
    */
   int getFarthest();
   
   /**
    * Enable/disable debugging window
    * @param enable Enable if "true", disable if "false"
    * @param delay Time delay in [ms] for @p OpenCV to handle signals ('0' to skip)
    */
   void setDebug(bool enable, int delay = 0);

   /**
    * When debug active, save snapshot of current detection
    * @param filename Image file
    * @return FALSE if any error occured
    */
   bool saveSnapshot(const char* filename);

private:
   CvMemStorage* _storage;
   CvSeq* _pointSeq;
   CvSeq* _point32fSeq;
   
   int _howMany;
   vector< target_t > _target;
   
   int _bufferLength;
   IplImage* _debugImage;
   IplImage* _tmpImg;
   IplImage* _grayImg;

   bool _debug;
   bool _delay;
   IplImage* _oldGrid;
   IplImage* _newGrid;
   IplImage* _mapImg;
   FILE* _dataFile;

   deque< gpc_polygon* > _S;  // set of free-spaces
   deque< Polygon*/*Polygon_2**/ > _Space;  // set of free-spaces
   int _steps;                // size of _S
   double _maxDistance;

private:
   template <class Point>
   inline double getDistance(Point p1, Point p2)
      { return sqrt(sqr(p1.x - p2.x) + sqr(p1.y - p2.y)); }
};

#endif
