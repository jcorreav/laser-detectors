/***************************************************************************
 *   Copyright (C) 2011 by Nicola Bellotto                                 *
 *   nbellotto@lincoln.ac.uk                                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef DETECTOR_H
#define DETECTOR_H


#include <string>


/**
Interface for people detection modules

@author Nicola Bellotto
*/


class DetectorException {
protected:
  std::string _errmsg;
  int _errno;
public:
  DetectorException(std::string message = "Detector exception", int error = 0)
    { _errmsg =  "<DetectorException>: " + message; _errno = error; }
  virtual ~DetectorException() {}
  virtual std::string getMessage() { return _errmsg; }
  virtual int getError() { return _errno; }
};
  


class Detector{
public:
   Detector();
   
   virtual ~Detector() = 0;
   
   virtual int getHowMany() = 0;
};

#endif
