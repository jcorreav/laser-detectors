/***************************************************************************
 *   Copyright (C) 2011 by Nicola Bellotto                                 *
 *   nbellotto@lincoln.ac.uk                                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef FACEDETECTOR_H
#define FACEDETECTOR_H


#include "detector.h"

#include "utility.h"
#include "opencv/cv.h"
#include "opencv/highgui.h"

#include <string>
#include <vector>


/**
People detection by face (vision)

@author Nicola Bellotto
*/


class FaceDetectorException : public DetectorException {
protected:
  std::string _errmsg;
  int _errno;
public:
  FaceDetectorException(std::string message = "General exception", int error = 0)
    { _errmsg =  "<FaceDetectorException>: " + message; _errno = error; }
//   FaceDetectorException(const char* message = "General exception", int error = 0)
//     { _errmsg =  "<FaceDetectorException>: " + std::string(message); _errno = error; }
  std::string getMessage() { return _errmsg; }
  int getError() { return _errno; }
};
  


class FaceDetector : public Detector
{
   typedef struct
   {
      double distance;     // [m]
      double bearing;      // [rad]
      double minbearing;   // [rad]
      double maxbearing;   // [rad]
      double elevation;    // [rad]
      double minelevation; // [rad]
      double maxelevation; // [rad]
      int u;               // [pixel]
      int v;               // [pixel]
      int radius;          // [pixel]
      CvRect bound;        // face's bound
   } target_t;

public:
   /**
    * Constructor
    * @param frameWidth Frame width
    * @param frameHeight Frame height
    * @param frameDepth Frame depth
    * @param frame Pointer to OpenCV image
    * @param fx Horizontal focal length (in pixels)
    * @param fx Vertical focal length (in pixels)
    * @param cascadeFileName File containing the description of a trained cascade classifier
    * @param bearingOffset Offset of the bearing (= 0 by default)
    * @param elevationOffset Offset of the elevation (= 0 by default)
    */
   FaceDetector(int frameWidth, int frameHeight, int frameDepth,
                double fx, double fy, const char* cascadeFileName,
                double bearingOffset = 0, double elevationOffset = 0)
      throw (FaceDetectorException);

   /**
    * DEPRECATED
    * Constructor
    * @param frameWidth Frame width
    * @param frameHeight Frame height
    * @param frameDepth Frame depth
    * @param frame Pointer to OpenCV image
    * @param fov Field of view of the camera, in [rad]
    * @param cascadeFileName File containing the description of a trained cascade classifier
    * @param bearingOffset Offset of the bearing (= 0 by default)
    * @param elevationOffset Offset of the elevation (= 0 by default)
    */
   FaceDetector(int frameWidth, int frameHeight, int frameDepth,
                double fov, const char* cascadeFileName,
                double bearingOffset = 0, double elevationOffset = 0)
      throw (FaceDetectorException);

   /**
    * Destructor
    * @return 
    */
   ~FaceDetector();

   // update using the whole image
   /**
    * Update the whole image to detect new faces
    * @param frame Pointer to the frame
    */
   void update(const IplImage* frame) throw (FaceDetectorException);

   /**
    * Update a particular region of the image to detect new faces
    * @param xPos X position of the upper-left corner
    * @param yPos Y position of the upper-left corner
    * @param xSize X size of the region
    * @param ySize Y size of the region
    */
   void update(const IplImage* frame,
               int xPos, int yPos, int xSize = 0, int ySize = 0)
      throw (FaceDetectorException);
   
   /**
    * Reset detector
    */
   void reset() {
      _howMany = 0;
      _target.clear();
   }
   
   /**
    * Get how many faces has been detected with the last update
    * @return Number of detected faces
    */
   int getHowMany();

   /**
    * !!! DEPRECATED !!!
    * Get the distance of the specified detected face (very very roughly!)
    * @param personID ID of the face (between 0 and "getHowMany()")
    * @return Distance of the IDth face
    */
   double getDistance(int personID) throw (FaceDetectorException);

   /**
    * Accessor function to convert from pixel to radiants (bearing/elevation)
    * @param x Pixel x
    * @param y Pixel y
    * @param bearing Bearing in [rad]
    * @param elevation Elevation in [rad]
    */
   inline void getPixel2Rad(int x, int y, double& bearing, double& elevation) {
      bearing = atan((_frameWidth / 2. - x) * _Kb) + _bOffset;
         while (bearing > M_PI/2) bearing -= M_PI;
         while (bearing <= -M_PI/2) bearing += M_PI;
      elevation = atan((y - _frameHeight / 2.) * _Ke) + _eOffset;
         while (elevation > M_PI/2) elevation -= M_PI;
         while (elevation <= -M_PI/2) elevation += M_PI;
   }
   
   /**
    * Accessor function to convert from radiants (bearing/elevation) to pixel
    * @param bearing Bearing in [rad]
    * @param elevation Elevation in [rad]
    * @param x Pixel x
    * @param y Pixel y
    */
   inline void getRad2Pixel(double bearing, double elevation, int& x, int& y) {
      x = (int)(_frameWidth/2. - tan(bearing - _bOffset)/_Kb);
      y = (int)(_frameHeight/2. + tan(elevation - _eOffset)/_Ke);
   }
    
   /**
    * Get the bearing (horizontal direction) of the specified detected face
    * @param personID ID of the face (between 0 and "getHowMany()")
    * @return Bearing of the IDth face
    */
   double getBearing(int personID) throw (FaceDetectorException);

   /**
    * DEPRECATED - Substituted by @p getBearing
    */
   double getDirection(int personID) throw (FaceDetectorException) { return getBearing(personID); }
   
   /**
    * Get the minimum bearing of the specified detected face, that is the left side of the face (or right side, from the camera point of view)
    * @param personID ID of the face (between 0 and "getHowMany()")
    * @return Minimum bearing of the IDth face
    */
   double getMinBearing(int personID) throw (FaceDetectorException);
   
   /**
    * Get the maximum bearing of the specified detected face, that is the right side of the face (or left side, from the camera point of view)
    * @param personID ID of the face (between 0 and "getHowMany()")
    * @return Maximum bearing of the IDth face
    */
   double getMaxBearing(int personID) throw (FaceDetectorException);
   
   /**
    * Get the vertical direction (elevation, tilt) of the specified detected face
    * @param personID ID of the face (between 0 and "getHowMany()")
    * @return Elevation of the IDth face
    */
   double getElevation(int personID) throw (FaceDetectorException);

   /**
    * DEPRECATED - Substituted by @p getElevation
    */
   double getTilt(int personID) throw (FaceDetectorException) { return getElevation(personID); }

   /**
    * Get the minimum elevation of the specified detected face, that is the top side of the face
    * @param personID ID of the face (between 0 and "getHowMany()")
    * @return Minimum elevation of the IDth face
    */
   double getMinElevation(int personID) throw (FaceDetectorException);
   
   /**
    * Get the maximum elevation of the specified detected face, that is the bottom side of the face
    * @param personID ID of the face (between 0 and "getHowMany()")
    * @return Maximum elevation of the IDth face
    */
   double getMaxElevation(int personID) throw (FaceDetectorException);
   
   /**
    * Get the 'u' coordinate (known also as 'x') in the image plane of the face
    * @param personID ID of the face (between 0 and "getHowMany()")
    * @return 'u' coordinate of the face
    */
   int getU(int personID) throw (FaceDetectorException);
   
   /**
    * Get the 'v' coordinate (known also as 'y') in the image plane of the face
    * @param personID ID of the face (between 0 and "getHowMany()")
    * @return 'v' coordinate of the face
    */
   int getV(int personID) throw (FaceDetectorException);
   
   /**
    * Get the radius in pixels of the face
    * @param personID ID of the face (between 0 and "getHowMany()")
    * @return Radius of the face
    */
   int getRadius(int personID) throw (FaceDetectorException);
   
   /**
    * Get the face's boundaries in the current image
    * @param personID ID of the face (between 0 and "getHowMany()")
    * @return Face's boundaries
    */
   CvRect getBound(int personID) throw (FaceDetectorException);
   
   /**
    * Get the closest detected face (bigger face)
    * @return ID of the closest face
    */
   int getClosest();

   /**
    * Enable/disable a simple face tracking for fast detection
    * @param enable Enable if "true", disable if "false"
    */
   void setTracking(bool enable);

   /**
    * Enable/disable debugging window
    * @param enable Enable if "true", disable if "false"
    * @param delay Time delay in [ms] for @p OpenCV to handle signals ('0' to skip)
    */
   void setDebug(bool enable, int delay = 0);

   /**
    * When debug active, save snapshot of current detection
    * @param filename Image file
    * @return FALSE if any error occured
    */
   bool saveSnapshot(const char* filename);

public:
   static const double CANON_VCC4_HAOV;
   static const double SONY_EVID30_HAOV;
   static const double LOGITECH_QUICKCAM_HAOV;
private:
   int _frameWidth;
   int _frameHeight;
   int _frameDepth;

   double _Kb;  // = inverse of the horizontal focal length in pixels
   double _Ke;  // = inverse of the vertical focal length in pixels
   double _bOffset;  // bearing offset
   double _eOffset;  // elevation offset

   int _howMany;
   int _closest;
   std::vector<target_t> _target;
   
   CvMemStorage* _storage;
   CvHaarClassifierCascade* _cascade;
   IplImage* _frameCopy;
   CvSize _minFaceSize;
   CvRect* _lastFace;

   bool _tracking;
   bool _debug;
   bool _delay;
};

#endif
