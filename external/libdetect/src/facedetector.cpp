/***************************************************************************
 *   Copyright (C) 2011 by Nicola Bellotto                                 *
 *   nbellotto@lincoln.ac.uk                                               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "facedetector.h"

#include <iostream>
#include <errno.h>


// quickcam zoom parameters:
// #define PAN_ANGLE 42.6     // pan scale factor
// #define TILT_ANGLE 36.0    // tilt scale factor

// robot's camera parameters:
// angle of view (pan):    53°
// angle of view (tilt):   41°

// #define DIST_SCALE 0.27    // distance scale factor (for quickcam zoom)
#define DIST_SCALE 0.22    // distance scale factor (for canon vcc4)
// #define PAN_ANGLE 53.0     // pan scale factor
// #define TILT_ANGLE 41.0    // tilt scale factor

#define MIN_FACE_SIZE 20      // minimum face size in pixel
#define TRACK_WIN_SCALE 2     // tracking window scale factor (referred to face size)
#define TRACK_FACE_SCALE 0.8  // min. face scale factor (referred to previous face size)


const double FaceDetector::CANON_VCC4_HAOV = DTOR(47.5);
const double FaceDetector::SONY_EVID30_HAOV = DTOR(48.8);
const double FaceDetector::LOGITECH_QUICKCAM_HAOV = DTOR(42.6);


FaceDetector::FaceDetector(int frameWidth, int frameHeight, int frameDepth,
                           double fx, double fy, const char* cascadeFileName,
                           double bearingOffset, double elevationOffset)
   throw (FaceDetectorException) : Detector()
{
   _howMany = 0;
   _closest = -1;
   _tracking = false;
   _debug = false;
   _lastFace = NULL;
   
   _cascade = (CvHaarClassifierCascade*)cvLoad(cascadeFileName, 0, 0, 0);
   if (_cascade)
   {
      _storage = cvCreateMemStorage(0);

      _frameWidth = frameWidth;
      _frameHeight = frameHeight;
      _frameDepth = frameDepth;

//       if (_frameWidth > (MIN_FACE_SIZE * 4))
//          _minFaceSize = cvSize(_frameWidth / 4, _frameWidth / 4);
//       else
         _minFaceSize = cvSize(MIN_FACE_SIZE, MIN_FACE_SIZE);

      if ((fx > 0) && (fy > 0)) {
         _Kb = 1./fx;  // inverse of the focal length in pixels
         _Ke = 1./fy;  // inverse of the focal length in pixels
         _bOffset = bearingOffset;
         _eOffset = elevationOffset;
      }
      else
         throw FaceDetectorException("Wrong value for the focal length in FaceDetector constructor", EINVAL);
   }
   else
      throw FaceDetectorException("Cannot load classifier cascade FaceDetector::FaceDetector(IplImage* frame, const char* cascadeFileName)", ENOENT);
}


FaceDetector::FaceDetector(int frameWidth, int frameHeight, int frameDepth,
                           double fov, const char* cascadeFileName,
                           double bearingOffset, double elevationOffset)
   throw (FaceDetectorException) : Detector()
{
   _howMany = 0;
   _closest = -1;
   _tracking = false;
   _debug = false;
   _lastFace = NULL;
   
   _cascade = (CvHaarClassifierCascade*)cvLoad(cascadeFileName, 0, 0, 0);
   if (_cascade)
   {
      _storage = cvCreateMemStorage(0);

      _frameWidth = frameWidth;
      _frameHeight = frameHeight;
      _frameDepth = frameDepth;

//       if (_frameWidth > (MIN_FACE_SIZE * 4))
//          _minFaceSize = cvSize(_frameWidth / 4, _frameWidth / 4);
//       else
         _minFaceSize = cvSize(MIN_FACE_SIZE, MIN_FACE_SIZE);

      if ((fov > 0) && (fov < (M_PI / 2.))) {
         _Kb = _Ke = tan(fov / 2.) / (_frameWidth / 2.);  // inverse of the focal length in pixels
         _bOffset = bearingOffset;
         _eOffset = elevationOffset;
      }
      else
         throw FaceDetectorException("Wrong value for the field of view in FaceDetector constructor", EINVAL);
   }
   else
      throw FaceDetectorException("Cannot load classifier cascade FaceDetector::FaceDetector(IplImage* frame, const char* cascadeFileName)", ENOENT);
}


FaceDetector::~FaceDetector()
{
   if (_lastFace != NULL)
      delete _lastFace;

   cvReleaseMemStorage(&_storage);

   if (_debug)
   {
      cvReleaseImage(&_frameCopy);
      cvDestroyWindow("Face detector");
   }
}

void FaceDetector::update(const IplImage* frame) throw (FaceDetectorException)
{
   if (_tracking && (_lastFace != NULL))
   {
      // tracking window size
      int xSize = _lastFace->width * TRACK_WIN_SCALE;
      int ySize = _lastFace->height * TRACK_WIN_SCALE;

      // tracking window position
      int xPos = _lastFace->x + (int)(_lastFace->width * (1 - TRACK_WIN_SCALE) / 2.);
      int yPos = _lastFace->y + (int)(_lastFace->height * (1 - TRACK_WIN_SCALE) / 2.);

      // adapt the minimum face size to look for
      if (_lastFace->width > ((double)(MIN_FACE_SIZE) / TRACK_FACE_SCALE))
         _minFaceSize.width = _minFaceSize.height = (int)(_lastFace->width * TRACK_FACE_SCALE);
      else  // the minimum size used for training is the minimum allowed
         _minFaceSize.width = _minFaceSize.height = MIN_FACE_SIZE;

      // update on the tracking window
      update(frame, xPos, yPos, xSize, ySize);
   }
   else
   {
      // scan (update on) the whole image
      update(frame, 0, 0);
   }
}


void FaceDetector::update(const IplImage* frame,
                          int xPos, int yPos, int xSize, int ySize)
   throw (FaceDetectorException)
{
   // check the pointer to the frame
   if (!frame)
      throw FaceDetectorException("Null pointer to frame in FaceDetector::update(...)", EINVAL);
   
   CvArr* window = NULL;

   // choose if updating a sub-region or the whole frame
   if ((xSize != 0) && (ySize != 0))
   {
      // verify the tracking window doesn't go over the frame limits
      // check right side
      if ((xPos + xSize) > _frameWidth)
         xSize = _frameWidth - xPos;
      // check bottom side
      if ((yPos + ySize) > _frameHeight)
         ySize = _frameHeight - yPos;
      // check left side
      if (xPos < 0)
      {
         xSize += xPos;
         xPos = 0;
      }
      // check top side
      if (yPos < 0)
      {
         ySize += yPos;
         yPos = 0;
      }

      // update on the tracking window
      window = cvCreateMatHeader(ySize, xSize, _frameDepth);
      cvGetSubRect(frame, (CvMat*)window, cvRect(xPos, yPos, xSize, ySize));
   }
   else
   {
      // reset the minimum face size to the default one
      _minFaceSize.width = _minFaceSize.height = MIN_FACE_SIZE;
      // scan (update on) the whole image
      window = (CvArr*)frame;
   }

   // detect the possible targets
   cvClearMemStorage(_storage);

   // default values for fast detection (see OpenCV documentation)
   CvSeq* faces = cvHaarDetectObjects(window, _cascade, _storage, 1.2, /*2*/3,
                                       CV_HAAR_DO_CANNY_PRUNING, _minFaceSize);

// "window" doen't need to be released, coz it just points to a portion of "frame"
//    if (window != frame)
//       cvReleaseMat((CvMat**)&window);

   if (_debug)
   {
      cvCopy(frame, _frameCopy);
   }

   // closest face ID and bounding box
   int closest = -1;
   CvRect biggestBound = cvRect(0,0,0,0);

   // empty target vector
   _target.clear();

   // if there is some target
   if ((_howMany = faces->total))
   {
      double minDist = DBL_MAX;

      // calculate and store the information for each target
      for (int i = 0; i < _howMany; i++)
      {
         CvRect* bound = (CvRect*)cvGetSeqElem(faces, i);

         target_t t;
         // calculate the distance of the face's image plane from the camera
         // and keep track of the minimum distance (closest face)
         if ((t.distance = DIST_SCALE * _frameHeight / (double)(bound->height)) < minDist)
         {
            minDist = t.distance;
            closest = i;
            biggestBound = *bound;
         }
         // calculate pan angle (left +, right -)
         t.minbearing = atan((_frameWidth / 2. - (xPos + bound->x + bound->width)) * _Kb) + _bOffset;
            while (t.minbearing > M_PI/2) t.minbearing -= M_PI;
            while (t.minbearing <= -M_PI/2) t.minbearing += M_PI;
         t.maxbearing = atan((_frameWidth / 2. - (xPos + bound->x)) * _Kb) + _bOffset;
            while (t.maxbearing > M_PI/2) t.maxbearing -= M_PI;
            while (t.maxbearing <= -M_PI/2) t.maxbearing += M_PI;
         t.bearing = (t.minbearing + t.maxbearing) / 2.;
            while (t.bearing > M_PI/2) t.bearing -= M_PI;
            while (t.bearing <= -M_PI/2) t.bearing += M_PI;
         // calculate the tilt angle (down +, up -)
         t.minelevation = atan(((yPos + bound->y) - _frameHeight / 2.) * _Ke) + _eOffset;
            while (t.minelevation > M_PI/2) t.minelevation -= M_PI;
            while (t.minelevation <= -M_PI/2) t.minelevation += M_PI;
         t.maxelevation = atan(((yPos + bound->y + bound->height) - _frameHeight / 2.) * _Ke) + _eOffset;
            while (t.maxelevation > M_PI/2) t.maxelevation -= M_PI;
            while (t.maxelevation <= -M_PI/2) t.maxelevation += M_PI;
         t.elevation = (t.minelevation + t.maxelevation) / 2.;
            while (t.elevation > M_PI/2) t.elevation -= M_PI;
            while (t.elevation <= -M_PI/2) t.elevation += M_PI;
         // coordinates in the image plane of the face centre
         t.u = xPos + bound->x + bound->width / 2;
         t.v = yPos + bound->y + bound->height / 2;
         t.radius = bound->width / 2;
         // face's image
         t.bound = cvRect(xPos + bound->x, yPos + bound->y, bound->width, bound->height);
         
         // add the current target to the vector
         _target.push_back(t);

         if (_debug)
         {
            CvPoint pt0, pt1, pt2;
            // draw the bounding rectangle (blue color)
            pt1.x = xPos + bound->x;
            pt2.x = xPos + bound->x + bound->width;
            pt1.y = yPos + bound->y;
            pt2.y = yPos + bound->y + bound->height;
            cvRectangle(_frameCopy, pt1, pt2, CV_RGB(0,0,255), 3, 8, 0);
//             #warning Change bound color and uncomment following lines
            pt0.x = (pt1.x + pt2.x) / 2;
            pt0.y = (pt1.y + pt2.y) / 2;
            cvLine(_frameCopy, cvPoint(pt0.x-5, pt0.y), cvPoint(pt0.x+5, pt0.y), CV_RGB(0,255,0), 1);
            cvLine(_frameCopy, cvPoint(pt0.x, pt0.y-5), cvPoint(pt0.x, pt0.y+5), CV_RGB(0,525,0), 1);
         }
      }
   }

   // update the (global) closest ID
   _closest = closest;
   // update the position of the last closest face
   if (_lastFace != NULL)
   {
      delete _lastFace;
      _lastFace = NULL;
   }
   if (_howMany > 0)
   {
      _lastFace = new CvRect;
      _lastFace->x = xPos + biggestBound.x;
      _lastFace->y = yPos + biggestBound.y;
      _lastFace->width = biggestBound.width;
      _lastFace->height = biggestBound.height;
   }

   if (_debug)
   {
      cvShowImage("Face detector", _frameCopy);
   //      char fname[64];
   //      sprintf(fname, "debug_face_%d.png", LegsDetector::debugStep);
   //      cvSaveImage(fname, _frameCopy);
      if (_delay)
        cvWaitKey(_delay);  // handles event processing of HIGHGUI library
   }
}


int FaceDetector::getHowMany()
{
   return _howMany;
}
    

double FaceDetector::getDistance(int personID) throw (FaceDetectorException)
{
   if ((personID >= 0) && (personID < _howMany))
      return _target[personID].distance;
   throw FaceDetectorException("Index out of boundaries in FaceDetector::getDistance(int personID)", EINVAL);
}
    

double FaceDetector::getBearing(int personID) throw (FaceDetectorException)
{
   if ((personID >= 0) && (personID < _howMany))
      return _target[personID].bearing;
   throw FaceDetectorException("Index out of boundaries in FaceDetector::getBearing(int personID)", EINVAL);
}


double FaceDetector::getMinBearing(int personID) throw (FaceDetectorException)
{
   if ((personID >= 0) && (personID < _howMany))
      return _target[personID].minbearing;
   throw FaceDetectorException("Index out of boundaries in FaceDetector::getMinBearing(int personID)", EINVAL);
}


double FaceDetector::getMaxBearing(int personID) throw (FaceDetectorException)
{
   if ((personID >= 0) && (personID < _howMany))
      return _target[personID].maxbearing;
   throw FaceDetectorException("Index out of boundaries in FaceDetector::getMaxBearing(int personID)", EINVAL);
}


double FaceDetector::getElevation(int personID) throw (FaceDetectorException)
{
   if ((personID >= 0) && (personID < _howMany))
      return _target[personID].elevation;
   throw FaceDetectorException("Index out of boundaries in FaceDetector::getElevation(int personID)", EINVAL);
}


double FaceDetector::getMinElevation(int personID) throw (FaceDetectorException)
{
   if ((personID >= 0) && (personID < _howMany))
      return _target[personID].minelevation;
   throw FaceDetectorException("Index out of boundaries in FaceDetector::getMinElevation(int personID)", EINVAL);
}


double FaceDetector::getMaxElevation(int personID) throw (FaceDetectorException)
{
   if ((personID >= 0) && (personID < _howMany))
      return _target[personID].maxelevation;
   throw FaceDetectorException("Index out of boundaries in FaceDetector::getMaxElevation(int personID)", EINVAL);
}


int FaceDetector::getU(int personID) throw (FaceDetectorException)
{
   if ((personID >= 0) && (personID < _howMany))
      return _target[personID].u;
   throw FaceDetectorException("Index out of boundaries in FaceDetector::getU(int personID)", EINVAL);
}


int FaceDetector::getV(int personID) throw (FaceDetectorException)
{
   if ((personID >= 0) && (personID < _howMany))
      return _target[personID].v;
   throw FaceDetectorException("Index out of boundaries in FaceDetector::getV(int personID)", EINVAL);
}


int FaceDetector::getRadius(int personID) throw (FaceDetectorException)
{
   if ((personID >= 0) && (personID < _howMany))
      return _target[personID].radius;
   throw FaceDetectorException("Index out of boundaries in FaceDetector::getRadius(int personID)", EINVAL);
}

   
CvRect FaceDetector::getBound(int personID) throw (FaceDetectorException)
{
   if ((personID >= 0) && (personID < _howMany))
      return _target[personID].bound;
   throw FaceDetectorException("Index out of boundaries in FaceDetector::getBound(int personID)", EINVAL);
}


int FaceDetector::getClosest()
{
   return _closest;
}


void FaceDetector::setTracking(bool enable)
{
   _tracking = enable;
}

void FaceDetector::setDebug(bool enable, int delay)
{
   if (!_debug && (_debug = enable))
   {
      _frameCopy = cvCreateImage(cvSize(_frameWidth, _frameHeight), IPL_DEPTH_8U, 3);
      cvNamedWindow("Face detector", 1);
   }
   _delay = delay;
}


bool FaceDetector::saveSnapshot(const char* filename)
{
   if (_debug && !cvSaveImage(filename, _frameCopy))
      return true;
   return false;
}
