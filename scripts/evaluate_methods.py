import numpy as np
import yaml
import math
import matplotlib.pyplot as plt
import matplotlib.animation as anim
import math as m
import sys
sys.path.append('./build/')
import laser_detectors

def animate(i):
    print("Processing frame {0}".format(i))
    xs = test_data[i, 1::3]
    ys = test_data[i, 2::3]
    ls = test_data[i, 3::3]

    ranges = np.sqrt(xs**2 + ys**2)
    angles = np.arctan2(ys, xs)

    # Should be sorted, but in their code they are doing this sorting.
    # Just to be safe I'm also sorting
    sorted_index = angles.argsort()

    sorted_ranges = ranges[sorted_index]
    sorted_angles = angles[sorted_index]
    sorted_labels = ls[sorted_index]

    # Detect
    detection = detector.detect([float(r) for r in sorted_ranges])
    if len(detection) > 0:
        legs_data = np.asarray(detection)
        data_legs.set_data(-legs_data[:,0] * np.cos(legs_data[:,1]), -legs_data[:,0] * np.sin(legs_data[:,1]))
    else:
        data_legs.set_data([], [])

    xs1 = xs[ls > 0.5]
    ys1 = ys[ls > 0.5]
    xs0 = xs[ls < 0.5]
    ys0 = ys[ls < 0.5]

    data_back.set_data(xs1, ys1)
    data_real.set_data(xs0, ys0)
    
    return data_back, data_real, data_legs

# Load testing data
test_data = np.loadtxt('external/people2D/data/testing.dat')

# Create a detector
#detector = laser_detectors.detector_lu("data/lu/trained_leg_detector.yaml")
#detector = laser_detectors.detector_spinello("data/spinello/ppl2Dindoor.C00_stumpboost.param_txt")
#detector = laser_detectors.detector_bellotto()
detector = laser_detectors.detector_vehicles()

# Basic properties
detector.num_measurements = int(360.0 / 2)
detector.min_angle = np.pi / 2.0
detector.angle_increment = m.radians(0.5)

# Distance to split clusters (spinello and lu)
dist_sq = 0.06**2

# Parameters for the spinello model
detector.sqjumpdist = dist_sq
detector.detector_threshold = 0.01

# Parameters for the lu model
detector.prob_threshold = 0.2
detector.cluster_threshold = dist_sq
detector.min_cluster_size = 5

# Vehicle (lines) detector
#detector.ransac_iterations = 500
#detector.percentage_remaining = 0.1
detector.cluster_threshold = 0.1*0.1
detector.distance_threshold = 0.1
detector.linearity_threshold = 0.4
detector.delta_touching = 1
detector.threshold_perpendicular = 1e-5

detector.initialise_detector()
# Create an empty plot for interactive rendering (animation)
f = plt.figure()
ax = f.gca()
ax.set_ylim(-10, 10)
ax.set_xlim(-10, 10)
data_back, = ax.plot([], [], 'k.')
data_real, = ax.plot([], [], 'r.')
data_legs, = ax.plot([], [], 'bo')

animation = anim.FuncAnimation(f, animate, xrange(len(test_data)))

plt.show()
