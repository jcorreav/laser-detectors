#ifndef _BELLOTTO_DETECTOR_H
#define _BELLOTTO_DETECTOR_H
#include "base_detector.h"
#include <legsdetector.h>
#include <memory>

namespace laser_detectors
{

struct detector_bellotto: base_detector
{
	using base_detector::num_measurements_;
	using base_detector::min_angle_;
	using base_detector::angle_increment_;
	int selectivity_;
	double filter_;

	std::shared_ptr<LegsDetector> detector_;

	detector_bellotto(): base_detector()
	{
		selectivity_ = 0;
		filter_ = 0.5;
	}

	virtual bool initialise_detector();

	virtual std::vector<std::pair<double, double>> detect(const std::vector<double> &ranges);

};

}
#endif

