#ifndef _SPINELLO_DETECTOR_H
#define _SPINELLO_DETECTOR_H
#include "base_detector.h"
#include <people2D_engine.hpp>
#include <memory>

namespace laser_detectors
{

struct detector_spinello: base_detector
{
	using base_detector::num_measurements_;
	using base_detector::min_angle_;
	using base_detector::angle_increment_;

	std::shared_ptr<LSL_lfeatures_class> lfeatures;
	LSL_boost_stage_param adaboost_stage;
	double sqjumpdist_;
	double detector_threshold_;

	detector_spinello(const std::string &model_file): base_detector()
	{
		sqjumpdist_ = 0.1;
		detector_threshold_ = 0.5;
		load_adaboost_model(model_file);

		std::vector<int> features(16);
		std::iota(features.begin(), features.end(), 0);
		lfeatures.reset(new LSL_lfeatures_class(features, std::vector<int>(1, 0)));
	}

	void predict(std::vector<std::vector<float>> descriptor, std::vector<float> &label_out);

	int get_breakpoint(std::vector <LSL_Point3D_str> &pts, int last_breaking_idx, double sqjumpdist);

	int segmentscanJDC(std::vector<LSL_Point3D_str> &laser, std::vector<LSL_Point3D_container> &clusters);

	int load_adaboost_model(const std::string &model_file);

	virtual bool initialise_detector();

	virtual std::vector<std::pair<double, double>> detect(const std::vector<double> &ranges);
};

}
#endif


