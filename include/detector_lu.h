#ifndef _LU_DETECTOR_H
#define _LU_DETECTOR_H
#include "base_detector.h"
#include <memory>
#include <Eigen/Core>
#include <Eigen/Dense>
#include <Eigen/StdVector>
#include <opencv/ml.h>

namespace laser_detectors
{

struct detector_lu: base_detector
{
	using base_detector::num_measurements_;
	using base_detector::min_angle_;
	using base_detector::angle_increment_;

	CvRTrees classifier_;
	double cluster_threshold_;
	size_t min_cluster_size_;
	double prob_threshold_;

	detector_lu(const std::string &model_file): base_detector()
	{
		classifier_.load(model_file.c_str());
	}


	Eigen::VectorXf calc_leg_features(const std::vector<size_t> &cluster_indices, const std::vector<Eigen::Vector4d, Eigen::aligned_allocator<Eigen::Vector4d>> &points);

	virtual bool initialise_detector();

	virtual std::vector<std::pair<double, double>> detect(const std::vector<double> &ranges);

};

}
#endif


