#ifndef _VEHICLE_DETECTOR_H
#define _VEHICLE_DETECTOR_H
#include "base_detector.h"
#include <memory>

namespace laser_detectors
{

struct detector_vehicles: base_detector
{
	using base_detector::num_measurements_;
	using base_detector::min_angle_;
	using base_detector::angle_increment_;

    double distance_threshold_;
#ifdef USE_RANSAC_LINE_DETECTOR
    size_t ransac_iterations_;
    double percentage_remaining_;
#else
	double cluster_threshold_;
	size_t min_cluster_size_;
    double linearity_threshold_;
#endif
    size_t delta_touching_;
    double threshold_perpendicular_;

	detector_vehicles(): base_detector()
	{
        distance_threshold_ = 0.06;
#ifdef USE_RANSAC_LINE_DETECTOR
        ransac_iterations_ = 100;
        percentage_remaining_ = 0.3;
#else
        cluster_threshold_ = 0.1;
        min_cluster_size_ = 10;
        linearity_threshold_ = 1.0;
#endif
        delta_touching_ = 1;
        threshold_perpendicular_ = 1E-5;
	}

	virtual bool initialise_detector();

	virtual std::vector<std::pair<double, double>> detect(const std::vector<double> &ranges);

};

}
#endif


