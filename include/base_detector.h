#ifndef _BASE_DETECTOR_H
#define _BASE_DETECTOR_H
#include <vector>
#include <cstddef>

namespace laser_detectors
{

struct base_detector
{
	size_t num_measurements_;
	double min_angle_, angle_increment_;

	base_detector()
	{
		num_measurements_ = 0;
		min_angle_ = 0;
		angle_increment_ = 0;
	}

	virtual bool initialise_detector() = 0;
	virtual std::vector<std::pair<double, double>> detect(const std::vector<double> &ranges) = 0;

};

}
#endif

