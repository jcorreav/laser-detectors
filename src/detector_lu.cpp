#include "detector_lu.h"
#include <opencv2/core/eigen.hpp>

using namespace laser_detectors;

Eigen::VectorXf detector_lu::calc_leg_features(const std::vector<size_t> &cluster_indices, const std::vector<Eigen::Vector4d, Eigen::aligned_allocator<Eigen::Vector4d>> &points)
{

	const size_t NUM_FEATURES = 14;
	Eigen::VectorXf features(NUM_FEATURES);
	size_t feat_id = 0;

	// Number of points
	int num_points = cluster_indices.size();
//	features(feat_id++) = num_points;

	// Compute mean and median points for future use
	float x_mean = 0.0;
	float y_mean = 0.0;
	std::vector<float> x_median_set;
	std::vector<float> y_median_set;
	for(const auto &idx: cluster_indices)
	{
		auto point = points[idx];
		x_mean += point(0) / num_points;
		y_mean += point(1) / num_points;
		x_median_set.push_back(point(0));
		y_median_set.push_back(point(1));
	}

	std::sort(x_median_set.begin(), x_median_set.end());
	std::sort(y_median_set.begin(), y_median_set.end());

	float x_median = 0.5 * (*(x_median_set.begin() + (num_points - 1) / 2) + * (x_median_set.begin() + num_points / 2));
	float y_median = 0.5 * (*(y_median_set.begin() + (num_points - 1) / 2) + * (y_median_set.begin() + num_points / 2));

	//Compute std and avg diff from median

	double sum_std_diff = 0.0;
	double sum_med_diff = 0.0;


	for(const auto &idx: cluster_indices)
	{
		auto point = points[idx];
		sum_std_diff += std::pow(point(0) - x_mean, 2) + std::pow(point(1) - y_mean, 2);
		sum_med_diff += std::sqrt(std::pow(point(0) - x_median, 2) + std::pow(point(1) - y_median, 2));
	}

	float std = std::sqrt(1.0 / (num_points - 1.0) * sum_std_diff);
	float avg_median_dev = sum_med_diff / num_points;

	features(feat_id++) = std;
	features(feat_id++) = avg_median_dev;

	// Compute Jump distance
	int prev_ind = cluster_indices.front() - 1;
	int next_ind = cluster_indices.back() + 1;

	float prev_jump = 0;
	float next_jump = 0;

	if(prev_ind >= 0)
	{
		prev_jump = (points[prev_ind].block(0,0,2,1) - points[cluster_indices.front()].block(0,0,2,1)).norm();
	}

	if(next_ind < (int)points.size())
	{
		next_jump = (points[next_ind].block(0,0,2,1) - points[cluster_indices.back()].block(0,0,2,1)).norm();
	}

	features(feat_id++) = prev_jump;
	features(feat_id++) = next_jump;

	// Compute Width
	float width = (points[cluster_indices.front()].block(0,0,2,1) - points[cluster_indices.back()].block(0,0,2,1)).norm();
	features(feat_id++) = width;

	// Compute Linearity

	Eigen::MatrixXd points_mat(num_points, 2);
	int j = 0;
	for(const auto &idx: cluster_indices)
	{
		auto point = points[idx];
		points_mat(j, 0) = point(0) - x_mean;
		points_mat(j, 1) = point(1) - y_mean;
		j++;
	}

	auto svd_points = points_mat.jacobiSvd(Eigen::ComputeFullU | Eigen::ComputeFullV);
	const auto &U = svd_points.matrixU();
	const auto &V = svd_points.matrixV();
	//const auto &s = svd_points.singularValues();

	Eigen::MatrixXd rot_points = U * V;

	float linearity = 0.0;
	for (int i = 0; i < num_points; i++)
	{
		linearity += std::pow(rot_points(i, 1), 2);
	}
	features(feat_id++) = linearity;

	// Compute Circularity
	Eigen::MatrixXd A(num_points, 3);
	Eigen::VectorXd B(num_points);
	j = 0;
	for(const auto &idx: cluster_indices)
	{
		auto point = points[idx];
		const float &x = point(0);
		const float &y = point(1);

		A(j, 0) = -2.0 * x;
		A(j, 1) = -2.0 * y;
		A(j, 2) = 1;

		B(j) = -std::pow(x, 2) - std::pow(y, 2);
		j++;
	}
	Eigen::VectorXd sol = A.jacobiSvd(Eigen::ComputeFullU | Eigen::ComputeFullV).solve(B);

	float xc = sol(0);
	float yc = sol(1);
	float rc = std::sqrt(std::pow(xc, 2) + std::pow(yc, 2) - sol(2));

	float circularity = 0.0;
	for(const auto &idx: cluster_indices)
	{
		auto point = points[idx];
		circularity += std::pow(rc - std::sqrt(std::pow(xc - point(0), 2) + std::pow(yc - point(1), 2)), 2);
	}

	features(feat_id++) = circularity;

	// Radius
	float radius = rc;

	features(feat_id++) = radius;

	//Curvature:
	float mean_curvature = 0.0;

	//Boundary length:
	float boundary_length = 0.0;
	float last_boundary_seg = 0.0;

	float boundary_regularity = 0.0;
	double sum_boundary_reg_sq = 0.0;

	// Mean angular difference
	auto left = cluster_indices.begin();
	left++;
	left++;
	auto mid = cluster_indices.begin();
	mid++;
	auto right = cluster_indices.begin();

	float ang_diff = 0.0;

	while(left != cluster_indices.end() && mid != cluster_indices.end())
	{
		const auto &point_left = points[*left];
		const auto &point_mid = points[*mid];
		const auto &point_right = points[*right];

		float mlx = point_left(0) - point_mid(0);
		float mly = point_left(1) - point_mid(1);
		float L_ml = std::sqrt(mlx * mlx + mly * mly);

		float mrx = point_right(0) - point_mid(0);
		float mry = point_right(1) - point_mid(1);
		float L_mr = std::sqrt(mrx * mrx + mry * mry);

		float lrx = point_left(0) - point_right(0);
		float lry = point_left(1) - point_right(1);
		float L_lr = std::sqrt(lrx * lrx + lry * lry);

		boundary_length += L_mr;
		sum_boundary_reg_sq += L_mr * L_mr;
		last_boundary_seg = L_ml;

		float A = (mlx * mrx + mly * mry) / std::pow(L_mr, 2);
		float B = (mlx * mry - mly * mrx) / std::pow(L_mr, 2);

		float th = std::atan2(B, A);

		if (th < 0)
			th += 2 * M_PI;

		ang_diff += th / num_points;

		float s = 0.5 * (L_ml + L_mr + L_lr);
		float area = std::sqrt(s * (s - L_ml) * (s - L_mr) * (s - L_lr));

		if (th > 0)
			mean_curvature += 4 * (area) / (L_ml * L_mr * L_lr * num_points);
		else
			mean_curvature -= 4 * (area) / (L_ml * L_mr * L_lr * num_points);

		left++;
		mid++;
		right++;
	}

	boundary_length += last_boundary_seg;
	sum_boundary_reg_sq += last_boundary_seg * last_boundary_seg;

	boundary_regularity = std::sqrt((sum_boundary_reg_sq - std::pow(boundary_length, 2) / num_points) / (num_points - 1));

	features(feat_id++) = boundary_length;
	features(feat_id++) = ang_diff;
	features(feat_id++) = mean_curvature;

	features(feat_id++) = boundary_regularity;


	// Mean angular difference
	auto first = cluster_indices.begin();
	mid = cluster_indices.begin();
	mid++;
	auto last = cluster_indices.end();
	last--;

	double sum_iav = 0.0;
	double sum_iav_sq  = 0.0;

	while(mid != cluster_indices.end() && mid != last)
	{
		const auto &point_first = points[*first];
		const auto &point_mid = points[*mid];
		const auto &point_last = points[*last];

		float mlx = point_first(0) - point_mid(0);
		float mly = point_first(1) - point_mid(1);
		//float L_ml = std::sqrt(mlx*mlx + mly*mly);

		float mrx = point_last(0) - point_mid(0);
		float mry = point_last(1) - point_mid(1);
		float L_mr = std::sqrt(mrx * mrx + mry * mry);

		//float lrx = (*first)->x - (*last)->x;
		//float lry = (*first)->y - (*last)->y;
		//float L_lr = std::sqrt(lrx*lrx + lry*lry);

		float A = (mlx * mrx + mly * mry) / std::pow(L_mr, 2);
		float B = (mlx * mry - mly * mrx) / std::pow(L_mr, 2);

		float th = std::atan2(B, A);

		if (th < 0)
			th += 2 * M_PI;

		sum_iav += th;
		sum_iav_sq += th * th;

		mid++;
	}

	float iav = sum_iav / num_points;
	float std_iav = std::sqrt((sum_iav_sq - std::pow(sum_iav, 2) / num_points) / (num_points - 1));

	features(feat_id++) = iav;
	features(feat_id++) = std_iav;

	return features;
}

bool detector_lu::initialise_detector()
{
	return true;
}

std::vector<std::pair<double, double>> detector_lu::detect(const std::vector<double> &ranges)
{
	// Create a xyzra 
	std::vector<Eigen::Vector4d, Eigen::aligned_allocator<Eigen::Vector4d>> points;
    points.reserve(ranges.size());
	for(size_t i = 0 ; i < ranges.size(); i++)
	{
		const double angle = min_angle_ + i * angle_increment_;
		points.push_back(Eigen::Vector4d(std::sin(angle) * ranges[i],
										 std::cos(angle) * ranges[i],
										 ranges[i],
										 angle));
	}

	// Split into clusters
	std::vector<std::vector<size_t>> clusters_indices;
	clusters_indices.resize(1);
	clusters_indices[0].push_back(0);
	size_t current_cluster = 0;
	for(size_t i = 1 ; i < ranges.size(); i++)
	{
		size_t last_indx = clusters_indices[current_cluster].back();
		auto dx = points[i].block(0,0,2,1) - points[last_indx].block(0,0,2,1);
		// If the points are close together, put them in the same cluster
		if(dx.squaredNorm() < cluster_threshold_)
		{
			clusters_indices[current_cluster].push_back(i);
		}
		// If points are far apart, create put the new point in a new cluster
		else
		{
			// If the last cluster is too small, reuse it as the new cluster
			if(clusters_indices[current_cluster].size() < min_cluster_size_)
			{
				clusters_indices[current_cluster].resize(1);
			}
			// Or create a new cluster
			else
			{
				current_cluster++;
				clusters_indices.push_back(std::vector<size_t>(1));
			}
			clusters_indices[current_cluster][0] = i;
		}
	}

    // The last cluster could have an invalid size
    if(clusters_indices.back().size() < min_cluster_size_)
    {
        clusters_indices.pop_back();
    }

	std::vector<std::pair<double, double>> result;
	for(const auto &cluster_indices: clusters_indices)
	{

		// Take features from clusters
		Eigen::VectorXf features = calc_leg_features(cluster_indices, points);

		// Make it a cv::Mat
		cv::Mat features_;
		cv::eigen2cv(features, features_);

		// Evaluate clusters
		double probability = classifier_.predict_prob(features_);

		// Add clusters with high probability to the result
		if(probability > prob_threshold_)
		{
			size_t mid_index = (cluster_indices.front() + cluster_indices.back() ) / 2;
            const double &range = points[mid_index](2);
            const double &angle = points[mid_index](3);
			result.push_back(std::make_pair(range, angle));
		}
	}
	return result;
}
