#include <boost/python.hpp>
#include "detector_bellotto.h"
#include "detector_spinello.h"
#include "detector_lu.h"
#include "detector_vehicles.h"

namespace p = boost::python;
using namespace laser_detectors;

// Helper function to adapt the input and output values
template<class Detector>
p::list detector_adapt(Detector &self, const p::list &ranges)
{
	std::vector<double> ranges_cpp(p::len(ranges));
	for(int i = 0; i < p::len(ranges); i++)
	{
		ranges_cpp[i] = p::extract<double>(ranges[i]);
	}
	// Do the detection
    std::vector<std::pair<double,double>> result = self.detect(ranges_cpp);

	p::list ret_list;
	for(const auto entry: result)
	{
		ret_list.append(p::make_tuple(entry.first, entry.second));
	}

	return ret_list;
}

BOOST_PYTHON_MODULE(laser_detectors)
{
	p::class_<detector_bellotto>("detector_bellotto")
        .def_readwrite("num_measurements", &detector_bellotto::num_measurements_)
        .def_readwrite("min_angle", &detector_bellotto::min_angle_)
        .def_readwrite("angle_increment", &detector_bellotto::angle_increment_)
        .def_readwrite("selectivity", &detector_bellotto::selectivity_)
        .def_readwrite("filter", &detector_bellotto::filter_)
		.def("initialise_detector", &detector_bellotto::initialise_detector)
		.def("detect", &detector_adapt<detector_bellotto>);

	p::class_<detector_spinello>("detector_spinello", p::init<const std::string &>())
        .def_readwrite("num_measurements", &detector_spinello::num_measurements_)
        .def_readwrite("min_angle", &detector_spinello::min_angle_)
        .def_readwrite("angle_increment", &detector_spinello::angle_increment_)
        .def_readwrite("sqjumpdist", &detector_spinello::sqjumpdist_)
        .def_readwrite("detector_threshold", &detector_spinello::detector_threshold_)
		.def("initialise_detector", &detector_spinello::initialise_detector)
		.def("detect", &detector_adapt<detector_spinello>);

	p::class_<detector_lu>("detector_lu", p::init<const std::string &>())
        .def_readwrite("num_measurements", &detector_lu::num_measurements_)
        .def_readwrite("min_angle", &detector_lu::min_angle_)
        .def_readwrite("angle_increment", &detector_lu::angle_increment_)
        .def_readwrite("cluster_threshold", &detector_lu::cluster_threshold_)
        .def_readwrite("min_cluster_size", &detector_lu::min_cluster_size_)
        .def_readwrite("prob_threshold", &detector_lu::prob_threshold_)
		.def("initialise_detector", &detector_lu::initialise_detector)
		.def("detect", &detector_adapt<detector_lu>);

	p::class_<detector_vehicles>("detector_vehicles")
        .def_readwrite("num_measurements", &detector_vehicles::num_measurements_)
        .def_readwrite("min_angle", &detector_vehicles::min_angle_)
        .def_readwrite("angle_increment", &detector_vehicles::angle_increment_)
        .def_readwrite("distance_threshold", &detector_vehicles::distance_threshold_)
#ifdef USE_RANSAC_LINE_DETECTOR
        .def_readwrite("ransac_iterations", &detector_vehicles::ransac_iterations_)
        .def_readwrite("percentage_remaining", &detector_vehicles::percentage_remaining_)
#else
        .def_readwrite("cluster_threshold", &detector_vehicles::cluster_threshold_)
        .def_readwrite("min_cluster_size", &detector_vehicles::min_cluster_size_)
        .def_readwrite("linearity_threshold", &detector_vehicles::linearity_threshold_)
#endif
        .def_readwrite("delta_touching", &detector_vehicles::delta_touching_)
        .def_readwrite("threshold_perpendicular", &detector_vehicles::threshold_perpendicular_)
		.def("initialise_detector", &detector_vehicles::initialise_detector)
		.def("detect", &detector_adapt<detector_vehicles>);
}
