#include "detector_bellotto.h"
#include "detector_spinello.h"
#include "detector_lu.h"
#include "detector_vehicles.h"

int main()
{
	laser_detectors::detector_bellotto d_b;
	laser_detectors::detector_spinello d_s("models/spinello/ppl2Dindoor.abt");
	laser_detectors::detector_lu d_l("models/lu/trained_leg_detector.yaml");

	std::vector<double> ranges;

	auto result_b = d_b.detect(ranges);
	auto result_s = d_s.detect(ranges);
	auto result_l = d_l.detect(ranges);
	return 0;
}
