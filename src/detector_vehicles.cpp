#include "detector_vehicles.h"
#include <cmath>
#include <utility>
#include <iostream>
#ifdef USE_RANSAC_LINE_DETECTOR
#include <pcl/ModelCoefficients.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/extract_indices.h>
#else
#include <Eigen/Dense>
#endif

using namespace laser_detectors;

struct line_entry
{
    Eigen::Vector3d model;
	std::pair<size_t, size_t> extremes;
};

bool detector_vehicles::initialise_detector()
{
	return true;
}

#if USE_RANSAC_LINE_DETECTOR
std::vector<std::pair<double, double>> detector_vehicles::detect(const std::vector<double> &ranges)
{
	// Convert to pcl
	pcl::PointCloud<pcl::PointXYZ>::Ptr pointcloud_laser(new pcl::PointCloud<pcl::PointXYZ>);
	pointcloud_laser->resize(ranges.size());
	pointcloud_laser->is_dense = true;
	pointcloud_laser->width = ranges.size();
	pointcloud_laser->height = 1;
	for(size_t i = 0; i < ranges.size(); i++)
	{
		const double &angle = min_angle_ + i * angle_increment_;
		const double &range = ranges[i];
		pointcloud_laser->points[i].x = range * std::cos(angle);
		pointcloud_laser->points[i].y = range * std::sin(angle);
		pointcloud_laser->points[i].z = 0.0;
	}

	// Extract all lines
	pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients());
	pcl::PointIndices::Ptr inliers(new pcl::PointIndices());
	// Create the segmentation object
	pcl::SACSegmentation<pcl::PointXYZ> seg;
	// Optional
	seg.setOptimizeCoefficients (true);
	// Mandatory
	seg.setModelType(pcl::SACMODEL_LINE);
	seg.setMethodType(pcl::SAC_RANSAC);
	seg.setMaxIterations(ransac_iterations_);
	seg.setDistanceThreshold(distance_threshold_);

	// Create the filtering object
	pcl::ExtractIndices<pcl::PointXYZ> extract;

	// Auxiliary clouds
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_f(new pcl::PointCloud<pcl::PointXYZ>);
	// Extreme points of the extracted lines
	std::vector<line_entry> found_lines;

	int i = 0;
	int nr_points = (int)pointcloud_laser->points.size();
	// While 30% of the original cloud is still there
	while(pointcloud_laser->points.size() > percentage_remaining_ * nr_points)
	{
		// Segment the largest planar component from the remaining cloud
		seg.setInputCloud(pointcloud_laser);
		seg.segment(*inliers, *coefficients);
		if(inliers->indices.size () == 0)
		{
			break;
		}

		// Extract the extreme points. The points shoyuld be sorted, but as I'm not
		// familiar with pcl's implementation, I rather take the minmax
		auto minmax = std::minmax_element(inliers->indices.begin(), inliers->indices.end());
		found_lines.resize(found_lines.size() + 1);
		found_lines.back().extremes = std::make_pair(*minmax.first, *minmax.second);
		std::copy(coefficients->values.begin(), coefficients->values.end(), std::back_inserter(found_lines.back().model));

        std::cout << "Found line with parameters: [";
        for(double m: found_lines.back().model)
        {
            std::cout << m << " ";
        }
        std::cout << std::endl;

		// Create the filtering object
		extract.setNegative(true);
        extract.setInputCloud(pointcloud_laser);
        extract.setIndices(inliers);
		extract.filter(*cloud_f);
		pointcloud_laser.swap (cloud_f);
		i++;
	}

	// Return vector
	std::vector<std::pair<double, double>> result;

	// Sort segments by the starting index
	std::sort(found_lines.begin(), found_lines.end(),
				[](const line_entry &one, const line_entry &two)
				{
					return one.extremes.first < two.extremes.first;
				});

	// Find connected which segments are connecte din L shape
	for(size_t i = 0; i < found_lines.size(); i++)
	{
		bool touching = false;
        bool perpendicular = false;
        if(found_lines.size() - 1 > 1)
        {
            touching = (found_lines[i+1].extremes.first - found_lines[i].extremes.second <= delta_touching_);
            double dot_prod = 0;
            for(size_t i = 0; i < 3; i++)
            {
                dot_prod += found_lines[i].model[i+3] * found_lines[i+1].model[i+3];
            }
		    perpendicular = std::fabs(dot_prod) < threshold_perpendicular_;
        }
		// Create output
		size_t middle_index = 0;
		double angle = 0, range = 0;
		if(touching && perpendicular)
		{
			middle_index = (found_lines[i].extremes.first + found_lines[i+1].extremes.second) / 2;
            // Have to skip the next one as it was used to complete the L-shape
            i++;
		}
		else
		{
			middle_index = (found_lines[i].extremes.first + found_lines[i].extremes.second) / 2;
		}
		angle = min_angle_ + middle_index * angle_increment_;
		range = ranges[middle_index];
		result.push_back(std::make_pair(range, angle));
	}
	return result;
}
#else
std::vector<std::pair<double, double>> detector_vehicles::detect(const std::vector<double> &ranges)
{
	// Create a xyzra 
	std::vector<Eigen::Vector4d, Eigen::aligned_allocator<Eigen::Vector4d>> points;
    points.reserve(ranges.size());
	for(size_t i = 0 ; i < ranges.size(); i++)
	{
		const double angle = min_angle_ + i * angle_increment_;
		points.push_back(Eigen::Vector4d(std::cos(angle) * ranges[i],
										 std::sin(angle) * ranges[i],
										 ranges[i],
										 angle));
	}

	// Split into line-clusters
    Eigen::MatrixXd A(1, 6);
    A(0, 0) = points[0](0);
    A(0, 1) = points[0](1);
    A(0, 2) = 1.0;
    A(0, 3) = points[0](0) * points[0](0);
    A(0, 4) = points[0](1) * points[0](1);
    A(0, 5) = points[0](0) * points[0](1);
    size_t j = 0;
	// Parameters of the found lines
	std::vector<line_entry> found_lines(1);
    found_lines.back().extremes.first = 0;
    found_lines.back().extremes.second = 0;
	for(size_t i = 1 ; i < ranges.size(); i++)
	{
		const size_t last_indx = i - 1;
		auto dx = points[i].block(0,0,2,1) - points[last_indx].block(0,0,2,1);

        // Add the point to the matrix
        A.conservativeResize(j + 1, 6);
        A(j, 0) = points[i](0);
        A(j, 1) = points[i](1);
        A(j, 2) = 1.0;
        A(j, 3) = points[i](0) * points[i](0);
        A(j, 4) = points[i](1) * points[i](1);
        A(j, 5) = points[i](0) * points[i](1);
        // If we have enought point estimate the line parameters
        bool linear = true;
        if(j > 1)
        {
            // Solve the least square problem
            const auto svdA = A.jacobiSvd(Eigen::ComputeFullU | Eigen::ComputeFullV);
            const auto &V = svdA.matrixV();
            const Eigen::VectorXd &params = V.col(5);
            double norm_line = params.block(0,0,2,1).norm();
            const Eigen::VectorXd normalised_line = params / norm_line;

            double dist_to_line = normalised_line.block(3,0,3,1).norm();
            linear = dist_to_line < linearity_threshold_;
        }
        j++;

		// If the points are far or not in a line, start a new cluster-line
        bool far = dx.squaredNorm() > cluster_threshold_;
		if(!linear || far)
		{
            if(far)
                j = 0;
            else
            {
                // If the points are not far, only non-linear, start the next segment at the previous point
                j = 1;
                i--;
            }
            // Check if its a straight line
            size_t num_points = found_lines.back().extremes.second - found_lines.back().extremes.first;
			// If the line was long enough, start a new line, otherwise, reuse the previous data to estimate the new line
			if(num_points >= min_cluster_size_)
            {
                // Solve the least square problem for line fitting
                const auto &Ap = A.block(0,0,A.rows(), 3);
                const auto svdAp = Ap.jacobiSvd(Eigen::ComputeFullU | Eigen::ComputeFullV);
                const auto &Vp = svdAp.matrixV();
                const Eigen::Vector3d &params = Vp.col(2);
                double norm_line = params.block(0,0,2,1).norm();
                const Eigen::Vector3d normalised_line = params / norm_line;
                double sqerr = (Ap * normalised_line).squaredNorm() / num_points;

                if(sqerr < distance_threshold_)
                {
                    found_lines.back().model = normalised_line;
                    found_lines.resize(found_lines.size() + 1);
                }
			}
            found_lines.back().extremes.first = i;

		}
        // Add this index to the current cluster
        found_lines.back().extremes.second = i;
	}

    // The last cluster could have an invalid size
    if(found_lines.back().extremes.second - found_lines.back().extremes.first < min_cluster_size_)
    {
        found_lines.pop_back();
    }

	// Return vector
	std::vector<std::pair<double, double>> result;
	// Find connected which segments are connecte din L shape
	for(size_t i = 0; i < found_lines.size(); i++)
	{
		bool touching = false;
        bool perpendicular = false;
        if(found_lines.size() - i > 1)
        {
            touching = (found_lines[i+1].extremes.first - found_lines[i].extremes.second <= delta_touching_);
            const double &dot_prod = found_lines[i].model.transpose() * found_lines[i+1].model;
		    perpendicular = std::fabs(dot_prod) < threshold_perpendicular_;
        }
		// Create output
        size_t start_index = 0;
        size_t end_index = 0;
		if(touching && perpendicular)
		{
            start_index = found_lines[i].extremes.first;
            end_index = found_lines[i+1].extremes.second;
            // Have to skip the next one as it was used to complete the L-shape
            i++;
		}
		else
		{
            start_index = found_lines[i].extremes.first;
            end_index = found_lines[i].extremes.second;
		}
		size_t middle_index = (start_index + end_index) / 2;
		double angle = min_angle_ + middle_index * angle_increment_;
		double range = 0;
        for(size_t k = start_index; k < end_index; k++)
        {
            range += ranges[k];
        }
        range /= (end_index - start_index);
		result.push_back(std::make_pair(range, angle));
	}
    return result;
}
#endif

