#include "detector_spinello.h"
#include "lfeatures.hpp"	
#include "lgeometry.hpp"	
#include <fstream>
#include <cmath>

using namespace laser_detectors;

void detector_spinello::predict(std::vector<std::vector<float>> descriptor, std::vector<float> &label_out)
{
 	unsigned int numsamples = descriptor.size();
	 
	//~ input vectors
	for (unsigned int i = 0; i < numsamples; i++)
	{
		double sumstump = 0;
		for ( unsigned int j = 0; j < adaboost_stage.model.size(); j++)
		{
			float class_label;
			if(descriptor[i][adaboost_stage.model[j].dimension] > adaboost_stage.model[j].theta ) 
				class_label = 1.0;
			else
				class_label = -1.0;	

			//~ hyperplane direction
			class_label  *= adaboost_stage.model[j].mode;
			
			//~ weighted sum
			sumstump += adaboost_stage.alpha[j] * class_label;
		}	
		
		label_out[i] = sumstump /  adaboost_stage.normalizer;
	}	
}

int detector_spinello::get_breakpoint(std::vector <LSL_Point3D_str> &pts, int last_breaking_idx, double sqjumpdist)
{
	int ptsz = pts.size() - 1;

	//~ failsafe
	int jmp_idx = pts.size(); 

	for(int i = last_breaking_idx; i < ptsz; i++)
	{
		double dist;
		dist = distance_L2_XY_sqr(&pts[i], &pts[i + 1]);

		if(dist > sqjumpdist)
		{ 
			//~ mark index
			jmp_idx = i + 1; 

			//~ bail out
			i = ptsz;
		}	
	}

	return(jmp_idx);
}

int detector_spinello::segmentscanJDC(std::vector<LSL_Point3D_str> &laser, std::vector<LSL_Point3D_container> &clusters)
{

	//~ bailing var
	char split_complete = 1;

	//~ numpts
	int ptsz = laser.size();

	//~ avoid nulls
	if(ptsz == 0)
		return (0);

	//~ last break point index
	int last_breaking_idx = 0;

	while(split_complete)
	{
		//~ min pts number
		if(last_breaking_idx < ptsz - 1)
		{
			//~ max distance check
			int breaking_idx = get_breakpoint(laser, last_breaking_idx, sqjumpdist_);

			if(breaking_idx - last_breaking_idx >= L_MINCLUSTERSZ)
			{
				//~ a cluster
				LSL_Point3D_container single_cluster;

				//~ pump it into
				single_cluster.pts.insert(single_cluster.pts.begin(), laser.begin() + last_breaking_idx, laser.begin() + breaking_idx);
				clusters.push_back(single_cluster);
			}	

			//~ endpoint
			last_breaking_idx = breaking_idx;
		}
		else
		{
			//~ break cycle
			split_complete = 0;
		}
	}

	return(1);
}

int detector_spinello::load_adaboost_model(const std::string &modelfile)
{
	std::ifstream ifs(modelfile.c_str());
	std::string line;
	 
	int count = 0;
	double anorm=0;
	while( getline( ifs, line ) )
	{  
		count++;
		
		std::vector<std::string> tokens;
		LSL_stringtoken(line, tokens, " ");

		if(tokens.size() != 13)
		{
			std::cout << "[LBSTBOOST]] Parsing failure in loading the boost file\n";
			exit(1);
		}	
		
		adaboost_stage.feattype.push_back( atoi(tokens[6].c_str()) );
		adaboost_stage.w.push_back( atof(tokens[7].c_str()) );
		adaboost_stage.alpha.push_back( atof(tokens[8].c_str()) );
		anorm += atof(tokens[8].c_str());
					
		LSL_weak_model mdl;
		mdl.theta = atof(tokens[9].c_str());
		mdl.mode = atoi(tokens[10].c_str());
		mdl.feature_idx = atoi(tokens[11].c_str());
		mdl.dimension = atoi(tokens[12].c_str());
		
		adaboost_stage.model.push_back(mdl);
		
	}
	adaboost_stage.normalizer = anorm;
		
 	return(count);
}

bool detector_spinello::initialise_detector()
{
	return true;
}

std::vector<std::pair<double, double>> detector_spinello::detect(const std::vector<double> &ranges)
{
	// The data structure use by this detector
	std::vector<LSL_Point3D_str> onescan(ranges.size());

	// Fill the laser data structure
	size_t i = 0;
	for(const double &range_i: ranges)
	{
		const double angle_i = min_angle_ + i * angle_increment_;
		onescan[i].x = range_i * std::cos(angle_i);
		onescan[i].y = range_i * std::sin(angle_i);
		onescan[i].label = 0;
		i++;
	}

	// order by theta. This should be ordered, but I'm keeping it to comply with the original code
	order_bytheta_incart(onescan);

	// Calculate features
	std::vector<std::vector<float>> descriptor;
	std::vector<LSL_Point3D_container> clusters;
	
	//~ compute segments
	segmentscanJDC(onescan, clusters);
	
	//~ compute descriptors
	lfeatures->compute_descriptor(clusters, descriptor);
	
	//~ associate label to segment
	std::vector<float> label(clusters.size());
	predict(descriptor, label);

	// Reserve the space for the output
	std::vector<std::pair<double, double>> result;
	result.reserve(clusters.size());
	// Helper origin point
	LSL_Point3D_str origin;	
	origin.x = 0;
	origin.y = 0;
	origin.z = 0;
	for(i = 0; i < clusters.size(); i++)
	{
		if(label[i] > detector_threshold_)
		{
			LSL_Point3D_str point;
			clusters[i].compute_cog(&point);
			const double range = distance_L2_XY(&point, &origin);
			const double angle = std::atan2(point.x, point.y);
			result.push_back(std::make_pair(range, angle));
		}
	}

	return result;
}
