#include "detector_bellotto.h"
#include <cmath>

using namespace laser_detectors;

bool detector_bellotto::initialise_detector()
{
	if(num_measurements_ > 0)
	{
		detector_.reset(new LegsDetector(num_measurements_, selectivity_, filter_));
		return true;
	}
	return false;
}

std::vector<std::pair<double, double>> detector_bellotto::detect(const std::vector<double> &ranges)
{
	// The data structure use by this detector
	vector<laser_t> laser_buf;

	// Fill the laser data structure
	laser_buf.resize(ranges.size());
	for(size_t i = 0; i < ranges.size(); i++)
	{
		const double range_i = ranges[i];
		const double angle_i = min_angle_ + i * angle_increment_;
		laser_buf[i].range = range_i;
		laser_buf[i].angle = angle_i;
		laser_buf[i].x = range_i * std::cos(angle_i);
		laser_buf[i].y = range_i * std::sin(angle_i);
		laser_buf[i].intensity = 1.0;
	}

	// Perform detection
	detector_->update(laser_buf);

	// Create the output
	const size_t new_size = detector_->getHowMany();

	// Reserve the space for the output
	std::vector<std::pair<double, double>> result;
	result.reserve(new_size);
	for(size_t i = 0; i < new_size; i++)
	{
		const double &range = detector_->getDistance(i);
		const double &angle = detector_->getBearing(i);
		result.push_back(std::make_pair(range, angle));
	}

	return result;
}
